package com.optime.listviewvideo.Adapter;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.optime.listviewvideo.Activity.CommentListActivity;
import com.optime.listviewvideo.Activity.LikesListActivity;
import com.optime.listviewvideo.Model.GetLatestPostRespose;
import com.optime.listviewvideo.Model.GetRankResponse;
import com.optime.listviewvideo.Model.Videos;
import com.optime.listviewvideo.R;
import com.optime.listviewvideo.Utility.CommonConstants;
import com.optime.listviewvideo.VideoPlayerModel.BaseVideoItem;
import com.optime.listviewvideo.VideoPlayerModel.VideoViewHolder;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersAdapter;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by danylo.volokh on 9/20/2015.
 */
public class VideoRecyclerViewAdapter extends RecyclerView.Adapter<VideoViewHolder> implements StickyRecyclerHeadersAdapter<RecyclerView.ViewHolder>{

    private final VideoPlayerManager mVideoPlayerManager;
    private final List<BaseVideoItem> mList;
    private final Context mContext;
   // private final ArrayList<String> tList;
    GetLatestPostRespose getLatestPostRespose;
    TextView tvName;
    TextView tvWeight;
    TextView tvLbsLifted;
    TextView tvRep;
    ImageView ivProfilePhoto;
    ImageView ivMore;
    RelativeLayout rrMore;
    TextView tvReport;
    TextView tvBlock;
    //String[][] list;
    String email;
    String token;
    String vid;
    int likeFlag=0;
    String userName;

    ArrayList<Videos>getVideosArrayList=new ArrayList<>();
    VideoViewHolder viewHolderNew = null;



    public VideoRecyclerViewAdapter(VideoPlayerManager videoPlayerManager, Context context, List<BaseVideoItem> list, ArrayList<Videos>getVideosArrayList){
   // public VideoRecyclerViewAdapter(VideoPlayerManager videoPlayerManager, Context context, List<BaseVideoItem> list, String[][] list1){
        mVideoPlayerManager = videoPlayerManager;
        mContext = context;
        mList = list;
        //this.tList = tList;
        //this.getLatestPostRespose=getLatestPostRespose;
        this.getVideosArrayList=getVideosArrayList;
    }

    @Override
    public VideoViewHolder onCreateViewHolder(ViewGroup viewGroup, int position) {
        BaseVideoItem videoItem = mList.get(position);
        View resultView = videoItem.createView(viewGroup, mContext.getResources().getDisplayMetrics().widthPixels);


        return new VideoViewHolder(resultView);
    }

    @Override
    public void onBindViewHolder(VideoViewHolder viewHolder, final int position) {

        try {
          //  TextView textView = (TextView) viewHolder.itemView;
            //String title=mVideoPlayerManager.toString();
           // textView.setText(tList.get(position));

            //viewHolder.titlevideo1.setText(tList.get(position));
            //viewHolder.textView.setText("iiiiii");
            viewHolderNew = viewHolder;
            BaseVideoItem videoItem = mList.get(position);
            videoItem.update(position, viewHolderNew, mVideoPlayerManager);


            SharedPreferences prefs = mContext.getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
            token= prefs.getString(CommonConstants.USER_SESSION_TOKEN, "");
            email= prefs.getString(CommonConstants.USER_EMAIL, "");


/*
            tvName.setText(tList.get(0));
            tvWeight.setText("Weight : "+tList.get(1)+" kg");
            tvLbsLifted.setText("   Lbs lifted : "+tList.get(2)+" ");
            tvRep.setText("   Reps"+tList.get(3));
*/
           // tvName.setText(list[position][0]);

/*
            String name=getLatestPostRespose.getVideos().get(position).getUser_name();
            String weight=getLatestPostRespose.getVideos().get(position).getUser_weight();
            String lbsLifted=getLatestPostRespose.getVideos().get(position).getWeight();
            String rep=getLatestPostRespose.getVideos().get(position).getRepetations();
            final String isLiked=getLatestPostRespose.getVideos().get(position).getIs_liked();
            String likeCount=getLatestPostRespose.getVideos().get(position).getVideo_likes();
            String commentCount=getLatestPostRespose.getVideos().get(position).getComments();
            vid=getLatestPostRespose.getVideos().get(position).getVid();
*/

            userName=getVideosArrayList.get(position).getUser_name();
            String weight=getVideosArrayList.get(position).getUser_weight();
            String lbsLifted=getVideosArrayList.get(position).getWeight();
            String rep=getVideosArrayList.get(position).getRepetations();
            final String isLiked=getVideosArrayList.get(position).getIs_liked();
            String likeCount=getVideosArrayList.get(position).getVideo_likes();
            String commentCount=getVideosArrayList.get(position).getComments();
            vid=getVideosArrayList.get(position).getVid();


            SharedPreferences sharedPref = mContext.getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(CommonConstants.VIDEO_ID, vid);
            editor.commit();

            tvName.setText(userName);
            tvWeight.setText("Weight : "+weight+" kg");
            tvLbsLifted.setText("  Lbs lifted : "+lbsLifted+" ");
            tvRep.setText("  Reps: "+rep);

            viewHolderNew.tvLikeCount.setText(likeCount+" Likes");
            viewHolderNew.tvCommentCount.setText(commentCount+ "Comments");


            if(isLiked.equals("0")){
                viewHolder.ivLike.setImageDrawable(mContext.getResources().getDrawable(R.drawable.like));

            }else{

                viewHolder.ivLike.setImageDrawable(mContext.getResources().getDrawable(R.drawable.after_like));
            }

            viewHolderNew.tvLikeCount.setText(likeCount+" Likes");

            viewHolderNew.ivLike.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (isLiked.equals("0")) {
                        likeFlag = 0;
                        //viewHolder.ivLike.setVisibility(View.GONE);
                        new LikeVideo(viewHolderNew, position).execute();

                    } else {
                        likeFlag = 1;
                        new LikeVideo(viewHolderNew, position).execute();
                    }
                }
            });

            viewHolderNew.tvLikeCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Intent intent=new Intent(mContext, LikesListActivity.class);
                    mContext.startActivity(intent);
                }
            });

            viewHolderNew.tvCommentCount.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {

                    Intent intent=new Intent(mContext, CommentListActivity.class);
                    mContext.startActivity(intent);
                }
            });

            ivMore.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    rrMore.setVisibility(View.VISIBLE);

                    tvReport.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            new UserReport().execute();
                        }
                    });

                    tvBlock.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            new UserBlock().execute();
                        }
                        });
                }
            });

        }catch (Exception e){
            e.printStackTrace();
        }
    }

    @Override
    public long getHeaderId(int position) {
        if (position == 0) {
            return 0;
        } else {
            //return mList.get(position).getVisibilityPercents(c);
            return position;
        }
    }

    @Override
    public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup parent) {
        View view = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.view_header, parent, false);
        tvName=(TextView)view.findViewById(R.id.view_header_title);
        tvWeight=(TextView)view.findViewById(R.id.tvWeight);
        tvLbsLifted=(TextView)view.findViewById(R.id.tvLbsLifted);
        tvRep=(TextView)view.findViewById(R.id.tvReps);
        ivProfilePhoto=(ImageView)view.findViewById(R.id.ivProfilePhoto);
        ivMore=(ImageView)view.findViewById(R.id.ivMore);
        rrMore=(RelativeLayout)view.findViewById(R.id.rrMore);
        tvReport=(TextView)view.findViewById(R.id.tvReport);
        tvBlock=(TextView)view.findViewById(R.id.tvBlock);

        return new RecyclerView.ViewHolder(view) {
        };
    }

    @Override
    public void onBindHeaderViewHolder(RecyclerView.ViewHolder holder, int position) {

        ////TextView textView = (TextView) holder.itemView;
        //textView.setText(tList.get(position));
       //// textView.setText(String.valueOf(getItem(position)));
        //holder.itemView.setBackgroundColor(getRandomColor());



    }
    public void remove(String object) {
        mList.remove(object);
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public String getItem(int position) {


        //return list.length;
        return null;

    }

    class LikeVideo extends AsyncTask<String, Void, String> {

        private Dialog loadingDialog;
        VideoViewHolder viewHolderNew1;
        int position;

        public LikeVideo(VideoViewHolder viewHolderNew, int position) {

            this.viewHolderNew1 = viewHolderNew;
            this.position = position;

        }

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // loadingDialog = ProgressDialog.show(MainActivity.this, "Please wait", "Loading...");

        }

        @Override
        protected String doInBackground(String... params) {

            InputStream is = null;
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("ent_email",email));
            nameValuePairs.add(new BasicNameValuePair("ent_token", "c0f3c97e289c529781e34b657b40713b"));
            nameValuePairs.add(new BasicNameValuePair("ent_vid", vid));

            String result = null;

            try{
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(
                        "http://optime.in/apps/mass_cast/mass_cast_api.php/like_video");
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = httpClient.execute(httpPost);

                HttpEntity entity = response.getEntity();

                is = entity.getContent();

                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();

                String line = null;
                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {

                try {

                    Gson gson = new Gson();
                    GetRankResponse getLikeVideoResponse= gson.fromJson(result, GetRankResponse.class);

                    if (getLikeVideoResponse.getErr().getCode() == 0) {

                        if(likeFlag==0) {
                            viewHolderNew1.ivLike.setImageDrawable(mContext.getResources().getDrawable(R.drawable.after_like));
                        }else{
                            viewHolderNew1.ivLike.setImageDrawable(mContext.getResources().getDrawable(R.drawable.like));
                        }
                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                }

            }
        }
    }

    class UserReport extends AsyncTask<String, Void, String> {

        private Dialog loadingDialog;
        VideoViewHolder viewHolderNew1;
        int position;



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // loadingDialog = ProgressDialog.show(MainActivity.this, "Please wait", "Loading...");

        }

        @Override
        protected String doInBackground(String... params) {

            InputStream is = null;
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("ent_email",email));
            nameValuePairs.add(new BasicNameValuePair("ent_token", "c0f3c97e289c529781e34b657b40713b"));
            nameValuePairs.add(new BasicNameValuePair("ent_partneruname", userName));
            nameValuePairs.add(new BasicNameValuePair("ent_reason", vid));

            String result = null;

            try{
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(
                        "http://optime.in/apps/mass_cast/mass_cast_api.php/report_user");
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = httpClient.execute(httpPost);

                HttpEntity entity = response.getEntity();

                is = entity.getContent();

                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();

                String line = null;
                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {

                try {

                    Gson gson = new Gson();
                    GetRankResponse getLikeVideoResponse= gson.fromJson(result, GetRankResponse.class);

                    if (getLikeVideoResponse.getErr().getCode() == 0) {

                        if(likeFlag==0) {
                            viewHolderNew1.ivLike.setImageDrawable(mContext.getResources().getDrawable(R.drawable.after_like));
                        }else{
                            viewHolderNew1.ivLike.setImageDrawable(mContext.getResources().getDrawable(R.drawable.like));
                        }
                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                }

            }
        }
    }

    class UserBlock extends AsyncTask<String, Void, String> {

        private Dialog loadingDialog;
        VideoViewHolder viewHolderNew1;
        int position;



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // loadingDialog = ProgressDialog.show(MainActivity.this, "Please wait", "Loading...");

        }

        @Override
        protected String doInBackground(String... params) {

            InputStream is = null;
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("ent_email",email));
            nameValuePairs.add(new BasicNameValuePair("ent_token", "c0f3c97e289c529781e34b657b40713b"));
            nameValuePairs.add(new BasicNameValuePair("ent_partneruname", userName));

            String result = null;

            try{
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(
                        "http://optime.in/apps/mass_cast/mass_cast_api.php/report_user");
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = httpClient.execute(httpPost);

                HttpEntity entity = response.getEntity();

                is = entity.getContent();

                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();

                String line = null;
                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {

                try {

                    Gson gson = new Gson();
                    GetRankResponse getLikeVideoResponse= gson.fromJson(result, GetRankResponse.class);

                    if (getLikeVideoResponse.getErr().getCode() == 0) {

                        if(likeFlag==0) {
                            viewHolderNew1.ivLike.setImageDrawable(mContext.getResources().getDrawable(R.drawable.after_like));
                        }else{
                            viewHolderNew1.ivLike.setImageDrawable(mContext.getResources().getDrawable(R.drawable.like));
                        }
                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                }

            }
        }
    }
}
