package com.optime.listviewvideo.Model;

/**
 * Created by optime on 28/6/16.
 */
public class UploadVideoResponse {

    String video_id;
    Error err;

    public Error getErr() {
        return err;
    }

    public void setErr(Error err) {
        this.err = err;
    }

    public String getVideo_id() {
        return video_id;
    }

    public void setVideo_id(String video_id) {
        this.video_id = video_id;
    }
}
