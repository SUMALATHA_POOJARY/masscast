package com.optime.listviewvideo.Model;

/**
 * Created by optime on 25/6/16.
 */
public class LoginResponse {

    User user;
    Error err;
    String token;

    public Error getErr() {
        return err;
    }

    public void setErr(Error err) {
        this.err = err;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }
}
