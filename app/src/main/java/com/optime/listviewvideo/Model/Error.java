package com.optime.listviewvideo.Model;

/**
 * Created by optime on 25/6/16.
 */
public class Error {

    int code;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    String msg;

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }
}
