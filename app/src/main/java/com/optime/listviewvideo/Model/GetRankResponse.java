package com.optime.listviewvideo.Model;

import java.util.ArrayList;

/**
 * Created by optime on 30/6/16.
 */
public class GetRankResponse {

    Error err;
    ArrayList<OverallRank> overall_rank=new ArrayList<>();
    ArrayList<GenderRank> gender_rank=new ArrayList<>();

    public ArrayList<GenderRank> getGender_rank() {
        return gender_rank;
    }

    public void setGender_rank(ArrayList<GenderRank> gender_rank) {
        this.gender_rank = gender_rank;
    }

    public ArrayList<OverallRank> getOverall_rank() {
        return overall_rank;
    }

    public void setOverall_rank(ArrayList<OverallRank> overall_rank) {
        this.overall_rank = overall_rank;
    }

    public Error getErr() {

        return err;
    }

    public void setErr(Error err) {
        this.err = err;
    }
}
