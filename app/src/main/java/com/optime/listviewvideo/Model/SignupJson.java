package com.optime.listviewvideo.Model;

/**
 * Created by optime on 25/6/16.
 */
public class SignupJson {
    String token;
    Error err;

    public Error getErr() {
        return err;
    }

    public void setErr(Error err) {
        this.err = err;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }
}
