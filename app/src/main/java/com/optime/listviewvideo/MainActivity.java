package com.optime.listviewvideo;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;

import com.optime.listviewvideo.Activity.HomePageActivity;
import com.optime.listviewvideo.Activity.LoginActivity;
import com.optime.listviewvideo.Utility.CommonConstants;

public class MainActivity extends AppCompatActivity {

    String session_token;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        SharedPreferences prefs = MainActivity.this.getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
        session_token= prefs.getString(CommonConstants.USER_SESSION_TOKEN, "");


        Thread timer = new Thread() {
            public void run() {
                try {
                    sleep(3000);


                } catch (Exception e) {
                    e.printStackTrace();
                } finally {


                    if(session_token==null||session_token.equals("")){       //||device_token.equals("")
                        Intent intent=new Intent(MainActivity.this, LoginActivity.class);
                        startActivity(intent);
                    }else {
                        Intent intent;
                        Intent i = new Intent(MainActivity.this, HomePageActivity.class);
                        startActivity(i);
                    }


                }

                MainActivity.this.finish();
            }
        };
        timer.start();
    }
}

        /*
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
*/