package com.optime.listviewvideo.Utility;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.res.AssetManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Point;
import android.graphics.Typeface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Environment;
import android.provider.MediaStore;
import android.util.Log;
import android.view.Display;
import android.view.WindowManager;

import com.cloudinary.Cloudinary;
import com.google.gson.Gson;
import com.optime.listviewvideo.Model.UploadVideoResponse;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.text.DateFormatSymbols;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

/**
 * Created by rmadan on 1/27/2015.
 */
public class Utility {

    public static double dp = 2;
    public static int screenHeight = 1080;
    public static int screenWidth = 720;

    public static int xxhdpiHeight = 1920;
    public static int xxhdpiWidth = 1080;

    public static int xxxhdpiHeight = 2560;
    public static int xxxhdpiWidth = 1440;

    public static int benchmarkWidth = xxxhdpiWidth;
    public static int benchmarkHeight = xxxhdpiHeight;

    public static String BASE_DIR =  Environment.getExternalStorageDirectory().getAbsolutePath();
    public static String CM_DIR = BASE_DIR  + "/WassUp/";
    public static String IMAGE_DIR = CM_DIR + "CMImages/";
    public static String AUDIO_DIR = CM_DIR + "CMAudios/";
    public static String VIDEO_DIR = CM_DIR + "CMVideos";
    public static String FILE_DIR = CM_DIR + "CMFiles/";
    public static String THUMBNAIL_DIR = CM_DIR + "CMThumbnails/";


    public static String VIDEO_URL ="";
    //public static String FILE_DIR = BASE_DIR + "CMFiles/";
    //public static String THUMBNAIL_DIR = FILE_DIR + "CMThumbnails/";

    public static String thumbUrl;
    public static String videoUrl;
    public static String token;
    public static String email;


    private static HashMap<String, String> monthHash;
    public static Cloudinary cloudinary;

    public static boolean isNetworkAvailable(Context context) {
        ConnectivityManager connectivityManager
                = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo activeNetworkInfo = connectivityManager.getActiveNetworkInfo();
        return activeNetworkInfo != null && activeNetworkInfo.isConnected();
    }

    public static void GetScreenDimensions(Context context) {

        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        Utility.screenHeight = height;
        Utility.screenWidth = width;
        int statusBarHeight = 0;
    }

    public static boolean ShowNetworkDialog(final Context context)
    {

        if (isNetworkAvailable(context)) {
            return true;
        }


        AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(context);
        alertDialogBuilder.setMessage("There seems to be no internet connectivity. Please check your internet connection and try again");
        alertDialogBuilder.setPositiveButton("Settings",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface arg0, int arg1) {

                     //   context.startActivityForResult(new Intent(android.provider.Settings.ACTION_SETTINGS), 0);
                        context.startActivity(new Intent(android.provider.Settings.ACTION_SETTINGS));

                    }
                });
        alertDialogBuilder.setNegativeButton("Cancel",
                new DialogInterface.OnClickListener() {

                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.dismiss();
                    }
                });

        AlertDialog alertDialog = alertDialogBuilder.create();
        try {
            alertDialog.show();
        } catch (Exception e) {

        }

        return false;
    }

    public static void GetScreenResolution(Context context) {
        WindowManager wm = (WindowManager) context.getSystemService(Context.WINDOW_SERVICE);
        Display display = wm.getDefaultDisplay();
        Point size = new Point();
        display.getSize(size);
        int width = size.x;
        int height = size.y;
        screenWidth = width;
        screenHeight = height;
        if (width < 350) {
            dp = 1;
           // Toast.makeText(context, " mdpi device",
           //         Toast.LENGTH_LONG).show();
        } else if (width >350 && width<510) {
            dp = 1.5;
           // Toast.makeText(context, " hdpi device",
           //         Toast.LENGTH_LONG).show();
        } else if (width > 510 && width < 820) {
            dp = 2;
           // Toast.makeText(context, " xhdpi device",
           //         Toast.LENGTH_LONG).show();
        } else if (width > 510 && width < 820) {
            dp = 3;
           // Toast.makeText(context, " xxhdpi device",
           //         Toast.LENGTH_LONG).show();
        } else {
            dp = 4;
        }
    }

    public static String getRealPathFromURI(Context context, Uri contentUri, int mediaType) {
        Cursor cursor = null;
        int column_index;
        try {
            String[] proj = { MediaStore.Images.Media.DATA };
            cursor = context.getContentResolver().query(contentUri,  proj, null, null, null);
            if (mediaType == 1) {
                column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            } else if (mediaType == 2) {
                column_index = cursor.getColumnIndexOrThrow(MediaStore.Video.Media.DATA);
            } else if (mediaType == 3) {
                column_index = cursor.getColumnIndexOrThrow(MediaStore.Files.FileColumns.DATA);
            } else {
                column_index = cursor.getColumnIndexOrThrow(MediaStore.Audio.Media.DATA);
            }
            cursor.moveToFirst();
            return cursor.getString(column_index);
        } finally {
            if (cursor != null) {
                cursor.close();
            }
        }
    }


    public static String ConvertTime(String date)
    {
        if (date != null) {
            String onlyDate = "";
            if (date.split("T").length > 1) {
                onlyDate = date.split("T")[1];
            } else if(date.split(" ").length > 1) {
                onlyDate = date.split(" ")[1];
            }
            int hourNum = Integer.valueOf(onlyDate.split(":")[0]);
            int min = Integer.valueOf(onlyDate.split(":")[1]);
            // String monthChar = new DateFormatSymbols().getMonths()[month-1];
            if (hourNum > 12) {
                hourNum = hourNum - 12;
                if (min < 10) {
                    return hourNum + ":0" + min + " AM";
                } else {
                    return hourNum + ":" + min + " PM";
                }
            } else {
                if (min < 10) {
                    return hourNum + ":0" + min + " am";
                } else {
                    return hourNum + ":" + min + " am";
                }
            }
        }
        return "";

    }


    public static String ConvertDate(String date)
    {

        if (monthHash != null) {

        } else {

            monthHash = new HashMap<String, String>();
            monthHash.put("January", "Jan");
            monthHash.put("February", "Feb");
            monthHash.put("March", "Mar");
            monthHash.put("April", "Apr");
            monthHash.put("May", "May");
            monthHash.put("June", "Jun");
            monthHash.put("July", "July");
            monthHash.put("August", "Aug");
            monthHash.put("September", "Sept");
            monthHash.put("October", "Oct");
            monthHash.put("November", "Nov");
            monthHash.put("December", "Dec");

        }
        if (date != null) {
            String onlyDate = "";
            if (date.split("T").length > 1) {
                onlyDate = date.split("T")[0];
            } else if(date.split(" ").length > 1) {
                onlyDate = date.split(" ")[0];
            }
            int dateNum = Integer.valueOf(onlyDate.split("-")[2]);
            int month = Integer.valueOf(onlyDate.split("-")[1]);
            String monthChar = new DateFormatSymbols().getMonths()[month - 1];
            return dateNum + " " + monthHash.get(monthChar);
        }
        return "";
    }

    public static String GetFileName(String path)
    {
        try {
            return path.split("/")[path.split("/").length - 1];
        }catch (Exception e) {
            return "";
        } finally {

        }
    }

    public static String getPath(Uri uri, Activity activity) {
        String[] projection = { MediaStore.MediaColumns.DATA };
        Cursor cursor = activity
                .managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.MediaColumns.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
    }

    public static int GenerateRandomNumber()
    {

        int max = 1000000;
        int min = 2;
        // NOTE: Usually this should be a field rather than a method
        // variable so that it is not re-seeded every call.
        Random rand = new Random();

        // nextInt is normally exclusive of the top value,
        // so add 1 to make it inclusive
        int randomNum = rand.nextInt((max - min) + 1) + min;

        return randomNum;
    }

    public static String getFilename(String folder) {
        //String filepath = Environment.getExternalStorageDirectory().getPath();
        String filepath = Environment.getExternalStorageDirectory() + File.separator
                + Environment.DIRECTORY_DCIM + File.separator + "FILE_NAME";
        File file = new File(filepath, folder);


        if (!file.exists()) {
            file.mkdirs();
        }

        return (file.getAbsolutePath() + "/" + System.currentTimeMillis() + "test.3gp");
    }



    public static void moveFile(String inputPath, String inputFile, String outputPath) {

        InputStream in = null;
        OutputStream out = null;
        try {

            //create output directory if it doesn't exist
            File dir = new File(outputPath);
            if (!dir.exists()) {
                dir.mkdirs();
            }


            in = new FileInputStream(inputPath);
            out = new FileOutputStream(outputPath + inputFile);

            byte[] buffer = new byte[1024];

            int read;
            while ((read = in.read(buffer)) != -1) {
                out.write(buffer, 0, read);
            }
            in.close();
            in = null;

            // write the output file
            out.flush();
            out.close();
            out = null;

            // delete the original file
            //   new File(inputPath).delete();


        } catch (Exception fnfe1) {
            Log.e("tag", fnfe1.getMessage());
        }

    }


    public static void ScaleFile(String inputPath, String inputFile, String outputPath) {
        try {
            File dir= new File(outputPath);

            Bitmap b= BitmapFactory.decodeFile(inputPath);
            if (b==null) {
                return;
            }
            int newx = 300;
            int newy;

            if (newx > b.getWidth()) {
                newx = b.getWidth();
            }
            newy = ((newx * b.getHeight()) / b.getWidth());
            Bitmap out = Bitmap.createScaledBitmap(b, newx, newy, false);

            File file = new File(dir, inputFile);
            FileOutputStream fOut;
            try {
                fOut = new FileOutputStream(file);
                out.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                fOut.flush();
                fOut.close();
                b.recycle();
                out.recycle();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        catch (Exception fnfe1) {
            Log.e("tag", fnfe1.getMessage());
        }

    }



    public static void ScaleThumbnailFile(String inputPath, String inputFile, String outputPath) {

        //      InputStream in = null;
        //      OutputStream out = null;
        try {


            File dir= new File(outputPath);

            Bitmap b= BitmapFactory.decodeFile(inputPath);
            int newx = 160;
            int newy;

            if (newx > b.getWidth()) {
                newx = b.getWidth();
            }
            newy = ((newx * b.getHeight()) / b.getWidth());
            Bitmap out = Bitmap.createScaledBitmap(b, newx, newy, false);

            File file = new File(dir, inputFile);
            FileOutputStream fOut;
            try {
                fOut = new FileOutputStream(file);
                out.compress(Bitmap.CompressFormat.PNG, 100, fOut);
                fOut.flush();
                fOut.close();
                b.recycle();
                out.recycle();
            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        catch (Exception fnfe1) {
            Log.e("tag", fnfe1.getMessage());
        }

    }


    public static void upload(final Context context,final String publicId ,final String filePath, final int type) {

        final Map[] uploadResult = new Map[1];
        final String[] uploadUrl = new String[1];
        final Map<String, String> options = new HashMap<>();
        if (type == 1 || type == 3) {
            options.put("public_id", publicId);

        } else {

            options.put("resource_type", "video");

        }

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {

                    InputStream in = null;
                    try {
                        Uri selectedImageUri = Uri.fromFile(new File(filePath));
                        in = context.getContentResolver().openInputStream(selectedImageUri);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }



                    uploadResult[0] = cloudinary.uploader().upload(in, options);

                    uploadUrl[0] = uploadResult[0].get("url").toString();




                    if(type==1){

                        thumbUrl=uploadUrl[0];
                        SharedPreferences sharedPref = context.getApplicationContext().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString(CommonConstants.THUMBNAIL_URL, String.valueOf(uploadUrl[0]));
                        editor.commit();
                    }else if(type==2){
                        videoUrl=uploadUrl[0];
                        SharedPreferences sharedPref = context.getApplicationContext().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
                        SharedPreferences.Editor editor = sharedPref.edit();
                        editor.putString(CommonConstants.VIDEO_URL, String.valueOf(uploadUrl[0]));
                        editor.commit();

                        SharedPreferences prefs = context.getApplicationContext().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
                        token= prefs.getString(CommonConstants.USER_SESSION_TOKEN, "");
                        email= prefs.getString(CommonConstants.USER_EMAIL, "");

                        new PostVideo().execute();

                    }



                } catch (IOException e) {
                    //TODO: better error handling when image uploading fails
                    e.printStackTrace();
                }catch(OutOfMemoryError e)
                {
                    e.printStackTrace();
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        };

        new Thread(runnable).start();
    }

    public static void uploadProfilePhoto(final Context context,final String publicId ,final String filePath, final int type) {

        final Map[] uploadResult = new Map[1];
        final String[] uploadUrl = new String[1];
        final Map<String, String> options = new HashMap<>();
        if (type == 1 || type == 3) {
            options.put("public_id", publicId);

        } else {

            options.put("resource_type", "video");

        }

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {

                    InputStream in = null;
                    try {
                        Uri selectedImageUri = Uri.fromFile(new File(filePath));
                        in = context.getContentResolver().openInputStream(selectedImageUri);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }



                    uploadResult[0] = cloudinary.uploader().upload(in, options);

                    uploadUrl[0] = uploadResult[0].get("url").toString();

                    SharedPreferences sharedPref = context.getApplicationContext().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(CommonConstants.PROFILE_PHOTO_URL, uploadUrl[0]);


                } catch (IOException e) {
                    //TODO: better error handling when image uploading fails
                    e.printStackTrace();
                }catch(OutOfMemoryError e)
                {
                    e.printStackTrace();
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        };

        new Thread(runnable).start();
    }




    public static Typeface AllerFont(Context context)
    {
        AssetManager mgr = (context).getAssets();

        Typeface font = Typeface.createFromAsset(mgr,
                "fonts/Aller_Std_Bd.ttf");
        return font;

    }

    public static Typeface AllerFontRegular(Context context)
    {
        AssetManager mgr = (context).getAssets();

        Typeface font = Typeface.createFromAsset(mgr,
                "fonts/Aller_Std_Rg.ttf");
        return font;

    }

    public static Typeface ABeeZeeRegular(Context context)
    {
        AssetManager mgr = (context).getAssets();

        Typeface font = Typeface.createFromAsset(mgr,
                "fonts/ABeeZee-Regular.otf");
        return font;

    }


    public static Typeface GlamatrixBold(Context context)
    {
        AssetManager mgr = (context).getAssets();

        Typeface font = Typeface.createFromAsset(mgr,
                "fonts/GlametrixBold.otf");
        return font;

    }


    public static Typeface GlamatrixRegular(Context context)
    {
        AssetManager mgr = (context).getAssets();

        Typeface font = Typeface.createFromAsset(mgr,
                "fonts/Glametrix.otf");
        return font;

    }

    public static Typeface GadugiBold(Context context)
    {
        AssetManager mgr = (context).getAssets();

        Typeface font = Typeface.createFromAsset(mgr,
                "fonts/gadugib.ttf");
        return font;

    }

    public static Typeface ArialRegular(Context context)
    {
        AssetManager mgr = (context).getAssets();

        Typeface font = Typeface.createFromAsset(mgr,
                "fonts/Chn_Prop_Arial_Normal.ttf");
        return font;

    }

    public static Typeface ShrutiRegular(Context context)
    {
        AssetManager mgr = (context).getAssets();

        Typeface font = Typeface.createFromAsset(mgr,
                "fonts/shruti.ttf");
        return font;

    }

    public static Typeface AllerItalic(Context context)
    {
        AssetManager mgr = (context).getAssets();

        Typeface font = Typeface.createFromAsset(mgr,
                "fonts/Aller_Std_It.ttf");
        return font;

    }

    public static Typeface HelveticaNeueMedium(Context context)
    {
        AssetManager mgr = (context).getAssets();

        Typeface font = Typeface.createFromAsset(mgr,
                "fonts/helvetica-neue-medium.ttf");
        return font;

    }


    public static Typeface CoolveticaRegular(Context context)
    {
        AssetManager mgr = (context).getAssets();

        Typeface font = Typeface.createFromAsset(mgr,
                "fonts/CoolveticaRg-Regular.ttf");
        return font;

    }

    public static Typeface RobotoRegular(Context context)
    {
        AssetManager mgr = (context).getAssets();

        Typeface font = Typeface.createFromAsset(mgr,
                "fonts/Roboto-Regular.ttf");
        return font;

    }

    public static Typeface RobotoLight(Context context)
    {
        AssetManager mgr = (context).getAssets();

        Typeface font = Typeface.createFromAsset(mgr,
                "fonts/Roboto-Light.ttf");
        return font;

    }


    public static Typeface RobotoBold(Context context)
    {
        AssetManager mgr = (context).getAssets();

        Typeface font = Typeface.createFromAsset(mgr,
                "fonts/Roboto-Bold.ttf");
        return font;

    }

    public static Typeface RobotoMedium(Context context)
    {
        AssetManager mgr = (context).getAssets();

        Typeface font = Typeface.createFromAsset(mgr,
                "fonts/Roboto-Medium.ttf");
        return font;

    }


    static class PostVideo extends AsyncTask<String, Void, String> {

        private Dialog loadingDialog;
        String result = null;



        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // loadingDialog = ProgressDialog.show(MainActivity.this, "Please wait", "Loading...");

        }

        @Override
        protected String doInBackground(String... params) {


            try {
               // String eid= URLEncoder.encode("1", "UTF-8");
               // String title=URLEncoder.encode("video1", "UTF-8");
              //  String videoUrl1= URLEncoder.encode(videoUrl, "UTF-8");
               // String thumb_url1=URLEncoder.encode(thumbUrl, "UTF-8");
              //  String weight=URLEncoder.encode("10", "UTF-8");
              //  String rep=URLEncoder.encode("3", "UTF-8");

             //   String vi=videoUrl;
             //   String th=thumbUrl;


                InputStream is = null;
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("ent_email", email));
                nameValuePairs.add(new BasicNameValuePair("ent_token", "c0f3c97e289c529781e34b657b40713b"));
                nameValuePairs.add(new BasicNameValuePair("ent_eid", "5"));
                nameValuePairs.add(new BasicNameValuePair("ent_title", "video5"));
                nameValuePairs.add(new BasicNameValuePair("ent_url", videoUrl+""));
                nameValuePairs.add(new BasicNameValuePair("ent_thumb_url", thumbUrl+""));
                nameValuePairs.add(new BasicNameValuePair("ent_weight", "50 "));
                nameValuePairs.add(new BasicNameValuePair("ent_rep", "10"));





                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(
                        "http://optime.in/apps/mass_cast/mass_cast_api.php/insert_video");
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = httpClient.execute(httpPost);

                HttpEntity entity = response.getEntity();

                is = entity.getContent();

                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();

                String line = null;
                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {


                Gson gson = new Gson();

                UploadVideoResponse uploadVideoResponse=gson.fromJson(result, UploadVideoResponse.class);

                if(uploadVideoResponse.getErr().getCode()==0){
                    String video_id=uploadVideoResponse.getVideo_id();
                }




                // }
            }
        }
    }




}


