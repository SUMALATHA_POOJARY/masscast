package com.optime.listviewvideo.Utility;

/**
 * Created by rmadan on 1/14/2015.
 */
public class CommonConstants {

    // public static String SERVER_URL = "http://104.131.62.36:8090/";
    public static String SERVER_URL = "http://vtnet.us:8010/";
    // public static String SERVER_URL = "http://128.199.156.112:9090/";
    //public static String SERVER_URL = "http://128.199.156.112:9080/";
    //public static String SERVER_URL = "http://128.199.156.112:9070/"; // v2
    // public static String SERVER_URL = "http://128.199.156.112:9060/"; // v2
    public static String USER_SETTINGS_PREFERENCE = "user_settings_preference";
    public static String USER_USER_NAME = "user_user_name";
    public static String USER_EMAIL = "user_email";
    public static String USER_PASSWORD = "user_password";
    public static String USER_NAME = "user_name";
    public static String USER_AGE = "user_age";
    public static String USER_SEX = "user_sex";
    public static String USER_DEVICE_ID = "user_devie_id";
    public static String USER_DEVICE_TYPE = "user_device_type";
    public static String USER_DEVICE_TOKEN = "user_token";
    public static String USER_SESSION_TOKEN = "session_token";
    public static String THUMBNAIL_URL = "thumbnail_url";
    public static String VIDEO_URL = "video_url";
    public static String USER_HEIGHT = "user_height";
    public static String USER_WEIGHT = "user_weight";
    public static String PROFILE_PHOTO_URL = "user_weight";
    public static String VIDEO_EXERCISE_ID = "video_exercise";
    public static String VIDEO_WEIGHT = "video_weight";
    public static String VIDEO_REP = "video_rep";
    public static String RANK_EXERCISE_ID = "rank_video_exercise";
    public static String VIDEO_ID = "video_id";




    // public static String SERVER_URL = "http://192.168.0.152:3000/";
}
