package com.optime.listviewvideo.Fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.widget.AbsListView;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.optime.listviewvideo.Activity.UploadActivity;
import com.optime.listviewvideo.VideoPlayerModel.BaseVideoItem;
import com.optime.listviewvideo.R;
import com.optime.listviewvideo.Utility.CommonConstants;
import com.optime.listviewvideo.VideoPlayerModel.VisibilityItem;
import com.volokh.danylo.video_player_manager.manager.PlayerItemChangeListener;
import com.volokh.danylo.video_player_manager.manager.SingleVideoPlayerManager;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;
import com.volokh.danylo.video_player_manager.meta.MetaData;
import com.volokh.danylo.visibility_utils.calculator.DefaultSingleItemCalculatorCallback;
import com.volokh.danylo.visibility_utils.calculator.ListItemsVisibilityCalculator;
import com.volokh.danylo.visibility_utils.calculator.SingleListViewItemActiveCalculator;
import com.volokh.danylo.visibility_utils.scroll_utils.ItemsPositionGetter;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;

/**
 * Created by optime on 27/6/16.
 */
public class UploadVideoFragment extends Fragment implements VisibilityItem.ItemCallback{

    String token;
    Dialog dialog;
    Button btnSubmit;

    private final ArrayList<BaseVideoItem> mList = new ArrayList<>();

    private final ListItemsVisibilityCalculator mVideoVisibilityCalculator =
            new SingleListViewItemActiveCalculator(new DefaultSingleItemCalculatorCallback(), mList);


    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;

    private ItemsPositionGetter mItemsPositionGetter;

    private final VideoPlayerManager<MetaData> mVideoPlayerManager = new SingleVideoPlayerManager(new PlayerItemChangeListener() {
        @Override
        public void onPlayerItemChanged(MetaData metaData) {

        }
    });

    private int mScrollState = AbsListView.OnScrollListener.SCROLL_STATE_IDLE;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        SharedPreferences prefs = getContext().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
        token= prefs.getString(CommonConstants.USER_SESSION_TOKEN, "");



        //new UploadVideo().execute();



        View rootView = inflater.inflate(R.layout.content_submit, container, false);

       // btnSubmit=(Button)rootView.findViewById(R.id.btnSubmit);

       // uploadVideo();


/*
        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadVideo();
            }
        });
*/
        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //mVisibilityUtilsCallback = (VisibilityUtilsFragment.VisibilityUtilsCallback)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        //mVisibilityUtilsCallback = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!mList.isEmpty()){
            // need to call this method from list view handler in order to have filled list

            mRecyclerView.post(new Runnable() {
                @Override
                public void run() {

                    mVideoVisibilityCalculator.onScrollStateIdle(
                            mItemsPositionGetter,
                            mLayoutManager.findFirstVisibleItemPosition(),
                            mLayoutManager.findLastVisibleItemPosition());


                }
            });
        }
    }




    @Override
    public void makeToast(String text) {

    }

    @Override
    public void onActiveViewChangedActive(View newActiveView, int newActiveViewPosition) {

    }

    public void uploadVideo(){
        dialog = new Dialog(getActivity());

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //tell the Dialog to use the dialog.xml as it's layout description

        dialog.setContentView(R.layout.gallery_camera_popup);
        dialog.setCancelable(true);

        RelativeLayout galleryLayout = (RelativeLayout) dialog.findViewById(R.id.gallery_layout);
        RelativeLayout cameraLayout = (RelativeLayout) dialog.findViewById(R.id.camera_layout);
        Button btnCancel=(Button) dialog.findViewById(R.id.btnCancel);

        galleryLayout.setOnClickListener(new RelativeLayout.OnClickListener() {
                                             @Override
                                             public void onClick(View v) {



                                                     if (ContextCompat.checkSelfPermission(getActivity(),
                                                             Manifest.permission.READ_EXTERNAL_STORAGE)
                                                             != PackageManager.PERMISSION_GRANTED) {

                                                         // Should we show an explanation?
                                                         if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                                                                 Manifest.permission.READ_EXTERNAL_STORAGE)) {

                                                             // Show an expanation to the user *asynchronously* -- don't block
                                                             // this thread waiting for the user's response! After the user
                                                             // sees the explanation, try again to request the permission.

                                                         } else {

                                                             // No explanation needed, we can request the permission.

                                                             ActivityCompat.requestPermissions(getActivity(),
                                                                     new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                                                     1);


                                                             // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                                                             // app-defined int constant. The callback method gets the
                                                             // result of the request.
                                                         }
                                                     } else {


                                                         Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT,
                                                                 android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                                         galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                                                         // Start the Intent

                                                         //Intent i = new Intent(Action.ACTION_MULTIPLE_PICK);
                                                         startActivityForResult(galleryIntent, 1);
                                                         dialog.dismiss();

                                                     }
                                                 }



                                         }

        );

        cameraLayout.setOnClickListener(new RelativeLayout.OnClickListener()

                                        {
                                            @Override
                                            public void onClick (View v){
                                                // dialog.dismiss();





                            /*if (ContextCompat.checkSelfPermission(getActivity(),
                                    Manifest.permission.CAMERA)
                                    != PackageManager.PERMISSION_GRANTED) {

                                // Should we show an explanation?
                                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                                        Manifest.permission.CAMERA)) {

                                    // Show an expanation to the user *asynchronously* -- don't block
                                    // this thread waiting for the user's response! After the user
                                    // sees the explanation, try again to request the permission.

                                } else {

                                    // No explanation needed, we can request the permission.

                                    ActivityCompat.requestPermissions(getActivity(),
                                            new String[]{Manifest.permission.CAMERA},
                                            2);

                                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                                    // app-defined int constant. The callback method gets the
                                    // result of the request.
                                }
                            } */

                          Intent intent1 = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                            File f = new File(android.os.Environment
                                    .getExternalStorageDirectory(), "temp.mp4");
                            startActivityForResult(intent1, 1);

                                                }

                                            }

    );

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == 1 && resultCode == Activity.RESULT_OK
                    && null != data) {


                String[] filePathColumn = { MediaStore.Images.Media.DATA };






                if(data.getData()!=null){

                    Uri mImageUri=data.getData();

                    // Get the cursor

                    String selectedImage = getPath(mImageUri);
                    Cursor cursor = getActivity().getContentResolver().query(mImageUri,
                            filePathColumn, null, null, null);

                    Intent intent=new Intent(getActivity(), UploadActivity.class);
                    intent.putExtra("imagePath", selectedImage);
                    startActivity(intent);
                    // Move to first row
                    cursor.moveToFirst();



                }


            }else if (requestCode == 2) {


                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");

                Uri tempUri = getImageUri(getActivity().getApplicationContext(), imageBitmap);

                // CALL THIS METHOD TO GET THE ACTUAL PATH
                File finalFile = new File(getRealPathFromURI(tempUri));
                File f = finalFile;






            }
            else {
                Toast.makeText(getContext(), "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(getContext(), "Something went wrong", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getActivity().getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public String getPath(Uri uri)
    {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = getActivity().managedQuery(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        return s;
    }
}
