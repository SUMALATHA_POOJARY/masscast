package com.optime.listviewvideo.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.optime.listviewvideo.Activity.RankExerciseActivity;
import com.optime.listviewvideo.Model.GenderRank;
import com.optime.listviewvideo.Model.GetRankResponse;
import com.optime.listviewvideo.Model.OverallRank;
import com.optime.listviewvideo.R;
import com.optime.listviewvideo.Utility.CommonConstants;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by optime on 30/6/16.
 */
public class RankFragment extends Fragment {

    String token;
    String email;
    String gender;
    TextView tvBench1RankNo;
    TextView tvSquat1RankNo;
    TextView tvDeadlift1RankNo;
    TextView tvCurls1RankNo;
    TextView tvIncline1RankNo;

    TextView tvBench2RankNo;
    TextView tvSquat2RankNo;
    TextView tvDeadlift2RankNo;
    TextView tvCurls2RankNo;
    TextView tvIncline2RankNo;

    TextView tvBench1Gender;
    TextView tvSquat1Gender;
    TextView tvdeadlift1Gender;
    TextView tvCurls1Gender;
    TextView tvIncline1Gender;

    RelativeLayout rrBench;
    RelativeLayout rrSquat;
    RelativeLayout rrDeadlift;
    RelativeLayout rrCurls;
    RelativeLayout rrIncline;



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.activity_rank, container, false);

        tvBench1RankNo=(TextView)rootView.findViewById(R.id.tvBench1RankNo);
        tvSquat1RankNo=(TextView)rootView.findViewById(R.id.tvSquat1RankNo);
        tvDeadlift1RankNo=(TextView)rootView.findViewById(R.id.tvDeadlift1RankNo);
        tvCurls1RankNo=(TextView)rootView.findViewById(R.id.tvCurls1RankNo);
        tvIncline1RankNo=(TextView)rootView.findViewById(R.id.tvIncline1RankNo);
        tvBench2RankNo=(TextView)rootView.findViewById(R.id.tvBench2RankNo);
        tvSquat2RankNo=(TextView)rootView.findViewById(R.id.tvSquat2RankNo);
        tvDeadlift2RankNo=(TextView)rootView.findViewById(R.id.tvDeadlift2RankNo);
        tvCurls2RankNo=(TextView)rootView.findViewById(R.id.tvCurls2RankNo);
        tvIncline2RankNo=(TextView)rootView.findViewById(R.id.tvIncline2RankNo);
        tvBench1Gender=(TextView)rootView.findViewById(R.id.tvBench1Gender);
        tvSquat1Gender=(TextView)rootView.findViewById(R.id.tvSquat1Gender);
        tvdeadlift1Gender=(TextView)rootView.findViewById(R.id.tvdeadlift1Gender);
        tvCurls1Gender=(TextView)rootView.findViewById(R.id.tvCurls1Gender);
        tvIncline1Gender=(TextView)rootView.findViewById(R.id.tvIncline1Gender);
        rrBench=(RelativeLayout)rootView.findViewById(R.id.rrBench);
        rrSquat=(RelativeLayout)rootView.findViewById(R.id.rrSquat);
        rrDeadlift=(RelativeLayout)rootView.findViewById(R.id.rrDeadlift);
        rrCurls=(RelativeLayout)rootView.findViewById(R.id.rrCurls);
        rrIncline=(RelativeLayout)rootView.findViewById(R.id.rrIncline);

        SharedPreferences prefs = getActivity().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
        token= prefs.getString(CommonConstants.USER_SESSION_TOKEN, "");
        email= prefs.getString(CommonConstants.USER_EMAIL, "");
        gender=prefs.getString(CommonConstants.USER_SEX, "");

        if(gender.equals("1")){
            tvBench1Gender.setText("MALE");
            tvSquat1Gender.setText("MALE");
            tvdeadlift1Gender.setText("MALE");
            tvCurls1Gender.setText("MALE");
            tvIncline1Gender.setText("MALE");
        }else{
            tvBench1Gender.setText("FEMALE");
            tvSquat1Gender.setText("FEMALE");
            tvdeadlift1Gender.setText("FEMALE");
            tvCurls1Gender.setText("FEMALE");
            tvIncline1Gender.setText("FEMALE");
        }


        new GetMyRank().execute();

        return rootView;
    }

    class GetMyRank extends AsyncTask<String, Void, String> {

        private Dialog loadingDialog;
        String result = null;




        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // loadingDialog = ProgressDialog.show(MainActivity.this, "Please wait", "Loading...");

        }

        @Override
        protected String doInBackground(String... params) {


            try {

                InputStream is = null;
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("ent_email", email));
                nameValuePairs.add(new BasicNameValuePair("ent_token", "c0f3c97e289c529781e34b657b40713b"));


                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(
                        "http://optime.in/apps/mass_cast/mass_cast_api.php/get_my_ranks");
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = httpClient.execute(httpPost);

                HttpEntity entity = response.getEntity();

                is = entity.getContent();

                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();

                String line = null;
                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {


                try {

                    Gson gson = new Gson();
                    GetRankResponse getRankResponse = gson.fromJson(result, GetRankResponse.class);

                    ArrayList<GenderRank> arrayList = new ArrayList<>();

                    if (getRankResponse.getErr().getCode() == 0) {

                        for (int i = 0; i < arrayList.size(); i++) {
                            tvBench1RankNo.setText(getRankResponse.getGender_rank().get(i).getRank_for_ex1());
                            tvSquat1RankNo.setText(getRankResponse.getGender_rank().get(i).getRank_for_ex2());
                            tvDeadlift1RankNo.setText(getRankResponse.getGender_rank().get(i).getRank_for_ex3());
                            tvCurls1RankNo.setText(getRankResponse.getGender_rank().get(i).getRank_for_ex4());
                            tvIncline1RankNo.setText(getRankResponse.getGender_rank().get(i).getRank_for_ex5());
                        }

                        ArrayList<OverallRank> arrayList1 = new ArrayList<>();

                        for (int i = 0; i < arrayList1.size(); i++) {

                            tvBench2RankNo.setText(getRankResponse.getGender_rank().get(i).getRank_for_ex1());
                            tvSquat2RankNo.setText(getRankResponse.getGender_rank().get(i).getRank_for_ex2());
                            tvDeadlift2RankNo.setText(getRankResponse.getGender_rank().get(i).getRank_for_ex3());
                            tvCurls2RankNo.setText(getRankResponse.getGender_rank().get(i).getRank_for_ex4());
                            tvIncline2RankNo.setText(getRankResponse.getGender_rank().get(i).getRank_for_ex5());
                        }
                    }

                    rrBench.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {

                            SharedPreferences sharedPref = getContext().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString(CommonConstants.RANK_EXERCISE_ID, "1");
                            editor.commit();

                            Intent intent = new Intent(getContext(), RankExerciseActivity.class);
                            startActivity(intent);
                        }
                    });

                    rrSquat.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SharedPreferences sharedPref = getContext().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString(CommonConstants.RANK_EXERCISE_ID, "2");
                            editor.commit();

                            Intent intent = new Intent(getContext(), RankExerciseActivity.class);
                            startActivity(intent);
                        }
                    });

                    rrDeadlift.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SharedPreferences sharedPref = getContext().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString(CommonConstants.RANK_EXERCISE_ID, "3");
                            editor.commit();

                            Intent intent = new Intent(getContext(), RankExerciseActivity.class);
                            startActivity(intent);
                        }
                    });

                    rrCurls.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SharedPreferences sharedPref = getContext().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString(CommonConstants.RANK_EXERCISE_ID, "4");
                            editor.commit();

                            Intent intent = new Intent(getContext(), RankExerciseActivity.class);
                            startActivity(intent);
                        }
                    });

                    rrIncline.setOnClickListener(new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            SharedPreferences sharedPref = getContext().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
                            SharedPreferences.Editor editor = sharedPref.edit();
                            editor.putString(CommonConstants.RANK_EXERCISE_ID, "5");
                            editor.commit();

                            Intent intent = new Intent(getContext(), RankExerciseActivity.class);
                            startActivity(intent);
                        }
                    });
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        }
    }

}
