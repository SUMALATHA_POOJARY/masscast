package com.optime.listviewvideo.Fragment;

import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;

import com.google.gson.Gson;
import com.optime.listviewvideo.Adapter.VideoRecyclerViewAdapter;
import com.optime.listviewvideo.Model.GetLatestPostRespose;
import com.optime.listviewvideo.Model.Videos;
import com.optime.listviewvideo.R;
import com.optime.listviewvideo.Utility.CommonConstants;
import com.optime.listviewvideo.VideoPlayerModel.BaseVideoItem;
import com.optime.listviewvideo.VideoPlayerModel.DividerDecoration;
import com.optime.listviewvideo.VideoPlayerModel.ItemFactory;
import com.optime.listviewvideo.VideoPlayerModel.VisibilityItem;
import com.optime.listviewvideo.VideoPlayerModel.VisibilityUtilsFragment;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersTouchListener;
import com.volokh.danylo.video_player_manager.Config;
import com.volokh.danylo.video_player_manager.manager.PlayerItemChangeListener;
import com.volokh.danylo.video_player_manager.manager.SingleVideoPlayerManager;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;
import com.volokh.danylo.video_player_manager.meta.MetaData;
import com.volokh.danylo.visibility_utils.calculator.DefaultSingleItemCalculatorCallback;
import com.volokh.danylo.visibility_utils.calculator.ListItemsVisibilityCalculator;
import com.volokh.danylo.visibility_utils.calculator.SingleListViewItemActiveCalculator;
import com.volokh.danylo.visibility_utils.scroll_utils.ItemsPositionGetter;
import com.volokh.danylo.visibility_utils.scroll_utils.RecyclerViewItemPositionGetter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * This fragment shows of how to use {@link VideoPlayerManager} with a RecyclerView.
 */
public class VideoRecyclerViewFragment extends Fragment implements VisibilityItem.ItemCallback {

    String email;
    String token;
    ArrayList<Videos> videosArrayList=new ArrayList<>();


    private static final boolean SHOW_LOGS = Config.SHOW_LOGS;
    private static final String TAG = VideoRecyclerViewFragment.class.getSimpleName();
    private VisibilityUtilsFragment.VisibilityUtilsCallback mVisibilityUtilsCallback;

    public VideoRecyclerViewFragment() {
    }



    private final ArrayList<BaseVideoItem> mList = new ArrayList<>();
    String[][] list;

    /**
     * Only the one (most visible) view should be active (and playing).
     * To calculate visibility of views we use {@link SingleListViewItemActiveCalculator}
     */
    private final ListItemsVisibilityCalculator mVideoVisibilityCalculator =
            new SingleListViewItemActiveCalculator(new DefaultSingleItemCalculatorCallback(), mList);


    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
  //  private TextView tvVideoTitle;
    /**
     * ItemsPositionGetter is used by {@link ListItemsVisibilityCalculator} for getting information about
     * items position in the RecyclerView and LayoutManager
     */
    private ItemsPositionGetter mItemsPositionGetter;

    /**
     * Here we use {@link SingleVideoPlayerManager}, which means that only one video playback is possible.
     */
    private final VideoPlayerManager<MetaData> mVideoPlayerManager = new SingleVideoPlayerManager(new PlayerItemChangeListener() {
        @Override
        public void onPlayerItemChanged(MetaData metaData) {

        }
    });

    private int mScrollState = AbsListView.OnScrollListener.SCROLL_STATE_IDLE;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {




        new GetLatestPost().execute();

/*
        try {
            mList.add(ItemFactory.createItemFromAsset("video_sample_1.mp4", R.drawable.video_sample_1_pic, getActivity(), mVideoPlayerManager));
            mList.add(ItemFactory.createItemFromAsset("video_sample_2.mp4", R.drawable.video_sample_2_pic, getActivity(), mVideoPlayerManager));
            mList.add(ItemFactory.createItemFromAsset("video_sample_3.mp4", R.drawable.video_sample_3_pic, getActivity(), mVideoPlayerManager));
            mList.add(ItemFactory.createItemFromAsset("video_sample_4.mp4", R.drawable.video_sample_4_pic, getActivity(), mVideoPlayerManager));

            mList.add(ItemFactory.createItemFromAsset("video_sample_1.mp4", R.drawable.video_sample_1_pic, getActivity(), mVideoPlayerManager));
            mList.add(ItemFactory.createItemFromAsset("video_sample_2.mp4", R.drawable.video_sample_2_pic, getActivity(), mVideoPlayerManager));
            mList.add(ItemFactory.createItemFromAsset("video_sample_3.mp4", R.drawable.video_sample_3_pic, getActivity(), mVideoPlayerManager));
            mList.add(ItemFactory.createItemFromAsset("video_sample_4.mp4", R.drawable.video_sample_4_pic, getActivity(), mVideoPlayerManager));

            mList.add(ItemFactory.createItemFromAsset("video_sample_1.mp4", R.drawable.video_sample_1_pic, getActivity(), mVideoPlayerManager));
            mList.add(ItemFactory.createItemFromAsset("video_sample_2.mp4", R.drawable.video_sample_2_pic, getActivity(), mVideoPlayerManager));
            mList.add(ItemFactory.createItemFromAsset("video_sample_3.mp4", R.drawable.video_sample_3_pic, getActivity(), mVideoPlayerManager));
            mList.add(ItemFactory.createItemFromAsset("video_sample_4.mp4", R.drawable.video_sample_4_pic, getActivity(), mVideoPlayerManager));
        } catch (IOException e) {
            throw new RuntimeException(e);
        }


*/
        View rootView = inflater.inflate(R.layout.fragment_video_recycler_view, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);
       // tvVideoTitle =  (TextView) rootView.findViewById(R.id.tvVideoTitle);
        //RecyclerViewHeader header = (RecyclerViewHeader) rootView.findViewById(R.id.header);

        // use this setting to improve performance if you know that changes
        // in content do not change the layout size of the RecyclerView
        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);




        //header.attachTo(mRecyclerView);


/*
        final ArrayList<String> tList = new ArrayList<String>();

        tList.add("video_sample_1.mp4");
        tList.add("video_sample_2.mp4");
        tList.add("video_sample_3.mp4");
        tList.add("video_sample_4.mp4");
        tList.add("video_sample_1.mp4");
        tList.add("video_sample_2.mp4");
        tList.add("video_sample_3.mp4");
        tList.add("video_sample_4.mp4");
        tList.add("video_sample_1.mp4");
        tList.add("video_sample_2.mp4");
        tList.add("video_sample_3.mp4");
        tList.add("video_sample_4.mp4");

*/




      /*  Arrays.asList(
                new VisibilityItem("video_sample_1.mp4", this),
                new VisibilityItem("video_sample_2.mp4", this),
                new VisibilityItem("video_sample_3.mp4", this),
                new VisibilityItem("video_sample_4.mp4", this),
                new VisibilityItem("video_sample_1.mp4", this),
                new VisibilityItem("video_sample_2.mp4", this),
                new VisibilityItem("video_sample_3.mp4", this),
                new VisibilityItem("video_sample_4.mp4", this),
                new VisibilityItem("video_sample_1.mp4", this),
                new VisibilityItem("video_sample_2.mp4", this),
                new VisibilityItem("video_sample_3.mp4", this),
                new VisibilityItem("video_sample_4.mp4", this)));  */

      //  final ListItemsVisibilityCalculator tListItemVisibilityCalculator =
           //     new SingleListViewItemActiveCalculator(new DefaultSingleItemCalculatorCallback(), tList);

        ItemsPositionGetter tItemsPositionGetter;

/*
        VideoRecyclerViewAdapter videoRecyclerViewAdapter = new VideoRecyclerViewAdapter(mVideoPlayerManager, getActivity(), mList, tList);

        mRecyclerView.setAdapter(videoRecyclerViewAdapter);

        final StickyRecyclerHeadersDecoration headersDecor = new StickyRecyclerHeadersDecoration(videoRecyclerViewAdapter);
        mRecyclerView.addItemDecoration(headersDecor);

        StickyRecyclerHeadersTouchListener touchListener =
                new StickyRecyclerHeadersTouchListener(mRecyclerView, headersDecor);
        touchListener.setOnHeaderClickListener(
                new StickyRecyclerHeadersTouchListener.OnHeaderClickListener() {
                    @Override
                    public void onHeaderClick(View header, int position, long headerId) {
                        Toast.makeText(getActivity(), "Header position: " + position + ", id: " + headerId,
                                Toast.LENGTH_SHORT).show();
                    }
                });
        mRecyclerView.addOnItemTouchListener(touchListener);

        videoRecyclerViewAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
            @Override
            public void onChanged() {
                headersDecor.invalidateHeaders();
            }
        });

        // Add decoration for dividers between list items
        mRecyclerView.addItemDecoration(new DividerDecoration(getContext()));
        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {
private ItemsPositionGetter mItemsPositionGetter;


            @Override
            public void onScrollStateChanged(RecyclerView recyclerView, int scrollState) {
                mScrollState = scrollState;
                if(scrollState == RecyclerView.SCROLL_STATE_IDLE && !mList.isEmpty()){

                    mVideoVisibilityCalculator.onScrollStateIdle(
                            mItemsPositionGetter,
                            mLayoutManager.findFirstVisibleItemPosition(),
                            mLayoutManager.findLastVisibleItemPosition());

                }
            }

            @Override
            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                if(!mList.isEmpty()){
                    mVideoVisibilityCalculator.onScroll(
                            mItemsPositionGetter,
                            mLayoutManager.findFirstVisibleItemPosition(),
                            mLayoutManager.findLastVisibleItemPosition() - mLayoutManager.findFirstVisibleItemPosition() + 1,
                            mScrollState);
                }
            }
        });
        mItemsPositionGetter = new RecyclerViewItemPositionGetter(mLayoutManager, mRecyclerView);

        */

        return rootView;
    }

    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        //mVisibilityUtilsCallback = (VisibilityUtilsFragment.VisibilityUtilsCallback)activity;
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mVisibilityUtilsCallback = null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!mList.isEmpty()){

            try {
                // need to call this method from list view handler in order to have filled list

                mRecyclerView.post(new Runnable() {
                    @Override
                    public void run() {

                        mVideoVisibilityCalculator.onScrollStateIdle(
                                mItemsPositionGetter,
                                mLayoutManager.findFirstVisibleItemPosition(),
                                mLayoutManager.findLastVisibleItemPosition());


                    }
                });

            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }

    class GetLatestPost extends AsyncTask<String, Void, String> {

        private Dialog loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // loadingDialog = ProgressDialog.show(MainActivity.this, "Please wait", "Loading...");

            //android_id = Settings.Secure.getString(LoginActivity.this.getContentResolver(),
                  //  Settings.Secure.ANDROID_ID);
        }

        @Override
        protected String doInBackground(String... params) {

            SharedPreferences prefs = getActivity().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
            token= prefs.getString(CommonConstants.USER_SESSION_TOKEN, "");
            email= prefs.getString(CommonConstants.USER_EMAIL, "");


            InputStream is = null;
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("ent_email", email));
            nameValuePairs.add(new BasicNameValuePair("ent_page", "1"));
            //nameValuePairs.add(new BasicNameValuePair("ent_device_id", android_id));
            nameValuePairs.add(new BasicNameValuePair("ent_page_size", "10"));
            nameValuePairs.add(new BasicNameValuePair("ent_token", "c0f3c97e289c529781e34b657b40713b" )); //token="ec043e7433dffb399ae4185a62fd606f"
            String result = null;

            try{
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(
                        "http://optime.in/apps/mass_cast/mass_cast_api.php/get_latest_post");
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = httpClient.execute(httpPost);

                HttpEntity entity = response.getEntity();

                is = entity.getContent();

                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();

                String line = null;
                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {

                try {

                    Gson gson = new Gson();
                    GetLatestPostRespose getLatestPostRespose = gson.fromJson(result, GetLatestPostRespose.class);

                    if (getLatestPostRespose.getErr().getCode() == 0) {

                        // String video=getLatestPostRespose.getVideos().;

                        ArrayList<Videos> arrayList = new ArrayList<>();
                        arrayList = getLatestPostRespose.getVideos();

                        videosArrayList = getLatestPostRespose.getVideos();

                        final ArrayList<String> tList = new ArrayList<String>();

                        try {
                            for (int i = 0; i < arrayList.size(); i++) {

                                String title = getLatestPostRespose.getVideos().get(i).getTitle();
                                String name = getLatestPostRespose.getVideos().get(i).getUser_name();
                                String weight = getLatestPostRespose.getVideos().get(i).getWeight();
                                String lbsLifted = getLatestPostRespose.getVideos().get(i).getWeight();
                                String rep = getLatestPostRespose.getVideos().get(i).getRepetations();
                                String image = getLatestPostRespose.getVideos().get(i).getThumbnail_url();
                                String video = getLatestPostRespose.getVideos().get(i).getVideo_url();
                                String profileUrl = getLatestPostRespose.getVideos().get(i).getUser_image();
                                String isLiked = getLatestPostRespose.getVideos().get(i).getIs_liked();
                                String likeCount = getLatestPostRespose.getVideos().get(i).getVideo_likes();
                                String commentCount = getLatestPostRespose.getVideos().get(i).getComments();
                                mList.add(ItemFactory.createItemFromAsset(title, video, image, getActivity(), mVideoPlayerManager));
                                tList.add(name);
                                tList.add(weight);
                                tList.add(lbsLifted);
                                tList.add(rep);
                                tList.add(profileUrl);

                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        VideoRecyclerViewAdapter videoRecyclerViewAdapter = new VideoRecyclerViewAdapter(mVideoPlayerManager, getActivity(), mList, arrayList);
                        // VideoRecyclerViewAdapter videoRecyclerViewAdapter = new VideoRecyclerViewAdapter(mVideoPlayerManager, getActivity(), mList, list);

                        mRecyclerView.setAdapter(videoRecyclerViewAdapter);

                        //HomePageAdapter homePageAdapter = new HomePageAdapter(mVideoPlayerManager, getActivity(), mList, tList);

                        //mRecyclerView.setAdapter(homePageAdapter);

                        final StickyRecyclerHeadersDecoration headersDecor = new StickyRecyclerHeadersDecoration(videoRecyclerViewAdapter);
                        mRecyclerView.addItemDecoration(headersDecor);

                        StickyRecyclerHeadersTouchListener touchListener =
                                new StickyRecyclerHeadersTouchListener(mRecyclerView, headersDecor);
                        touchListener.setOnHeaderClickListener(
                                new StickyRecyclerHeadersTouchListener.OnHeaderClickListener() {
                                    @Override
                                    public void onHeaderClick(View header, int position, long headerId) {
                                     //   Toast.makeText(getActivity(), "Header position: " + position + ", id: " + headerId,
                                        //        Toast.LENGTH_SHORT).show();
                                    }
                                });
                        mRecyclerView.addOnItemTouchListener(touchListener);

                        videoRecyclerViewAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                            @Override
                            public void onChanged() {
                                headersDecor.invalidateHeaders();
                            }
                        });

                        // Add decoration for dividers between list items
                        mRecyclerView.addItemDecoration(new DividerDecoration(getContext()));
                        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {


                            @Override
                            public void onScrollStateChanged(RecyclerView recyclerView, int scrollState) {
                                mScrollState = scrollState;
                                if (scrollState == RecyclerView.SCROLL_STATE_IDLE && !mList.isEmpty()) {

                                    mVideoVisibilityCalculator.onScrollStateIdle(
                                            mItemsPositionGetter,
                                            mLayoutManager.findFirstVisibleItemPosition(),
                                            mLayoutManager.findLastVisibleItemPosition());

                                }
                            }

                            @Override
                            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                try {
                                    if (!mList.isEmpty()) {
                                        mVideoVisibilityCalculator.onScroll(
                                                mItemsPositionGetter,
                                                mLayoutManager.findFirstVisibleItemPosition(),
                                                mLayoutManager.findLastVisibleItemPosition() - mLayoutManager.findFirstVisibleItemPosition() + 1,
                                                mScrollState);
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });
                        mItemsPositionGetter = new RecyclerViewItemPositionGetter(mLayoutManager, mRecyclerView);


                        // SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
                        //SharedPreferences.Editor editor = sharedPref.edit();
                        // editor.putString(CommonConstants.USER_TOKEN, loginResponse.getUser().getUser_token());

                        //editor.commit();


                    }
                }catch (Exception ex){
                    ex.printStackTrace();
                }
            }
        }
    }


    @Override
    public void onStop() {
        super.onStop();
        // we have to stop any playback in onStop
      //  mVideoPlayerManager.resetMediaPlayer();
    }

    @Override
    public void makeToast(String text) {

    }

    @Override
    public void onActiveViewChangedActive(View newActiveView, int newActiveViewPosition) {
       // mVisibilityUtilsCallback.setTitle("Active view at position " + newActiveViewPosition);

    }

}