package com.optime.listviewvideo.Fragment;

import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AbsListView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.optime.listviewvideo.Adapter.LikedVideosAdapter;
import com.optime.listviewvideo.Model.GetLatestPostRespose;
import com.optime.listviewvideo.Model.Videos;
import com.optime.listviewvideo.R;
import com.optime.listviewvideo.Utility.CommonConstants;
import com.optime.listviewvideo.VideoPlayerModel.BaseVideoItem;
import com.optime.listviewvideo.VideoPlayerModel.DividerDecoration;
import com.optime.listviewvideo.VideoPlayerModel.ItemFactory;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersDecoration;
import com.timehop.stickyheadersrecyclerview.StickyRecyclerHeadersTouchListener;
import com.volokh.danylo.video_player_manager.manager.PlayerItemChangeListener;
import com.volokh.danylo.video_player_manager.manager.SingleVideoPlayerManager;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;
import com.volokh.danylo.video_player_manager.meta.MetaData;
import com.volokh.danylo.visibility_utils.calculator.DefaultSingleItemCalculatorCallback;
import com.volokh.danylo.visibility_utils.calculator.ListItemsVisibilityCalculator;
import com.volokh.danylo.visibility_utils.calculator.SingleListViewItemActiveCalculator;
import com.volokh.danylo.visibility_utils.scroll_utils.ItemsPositionGetter;
import com.volokh.danylo.visibility_utils.scroll_utils.RecyclerViewItemPositionGetter;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by optime on 2/7/16.
 */
public class GetLikedVideosFragment extends Fragment {

    private final ArrayList<BaseVideoItem> mList = new ArrayList<>();
    private RecyclerView mRecyclerView;
    private LinearLayoutManager mLayoutManager;
    private ItemsPositionGetter mItemsPositionGetter;
    String email;
    String token;



    private final ListItemsVisibilityCalculator mVideoVisibilityCalculator =
            new SingleListViewItemActiveCalculator(new DefaultSingleItemCalculatorCallback(), mList);

    private final VideoPlayerManager<MetaData> mVideoPlayerManager = new SingleVideoPlayerManager(new PlayerItemChangeListener() {
        @Override
        public void onPlayerItemChanged(MetaData metaData) {

        }
    });

    private int mScrollState = AbsListView.OnScrollListener.SCROLL_STATE_IDLE;
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_video_recycler_view, container, false);

        mRecyclerView = (RecyclerView) rootView.findViewById(R.id.recycler_view);

        mRecyclerView.setHasFixedSize(true);

        // use a linear layout manager
        mLayoutManager = new LinearLayoutManager(getActivity());
        mRecyclerView.setLayoutManager(mLayoutManager);

        SharedPreferences prefs = getActivity().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
        token= prefs.getString(CommonConstants.USER_SESSION_TOKEN, "");
        email= prefs.getString(CommonConstants.USER_EMAIL, "");


        new GetLikedVideos().execute();


        return rootView;
    }

    class GetLikedVideos extends AsyncTask<String, Void, String> {

        private Dialog loadingDialog;
        String result = null;




        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // loadingDialog = ProgressDialog.show(MainActivity.this, "Please wait", "Loading...");

        }

        @Override
        protected String doInBackground(String... params) {


            try {

                InputStream is = null;
                List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
                nameValuePairs.add(new BasicNameValuePair("ent_email", email));
                nameValuePairs.add(new BasicNameValuePair("ent_token", "c0f3c97e289c529781e34b657b40713b"));
                nameValuePairs.add(new BasicNameValuePair("ent_page", "1"));
                nameValuePairs.add(new BasicNameValuePair("ent_page_size", "10"));


                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(
                        "http://optime.in/apps/mass_cast/mass_cast_api.php/get_liked_videos");
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = httpClient.execute(httpPost);

                HttpEntity entity = response.getEntity();

                is = entity.getContent();

                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();

                String line = null;
                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            try {

                if (result != null) {


                    Gson gson = new Gson();
                    GetLatestPostRespose getLatestPostRespose = gson.fromJson(result, GetLatestPostRespose.class);

                    if (getLatestPostRespose.getErr().getCode() == 0) {

                        // String video=getLatestPostRespose.getVideos().;

                        ArrayList<Videos> arrayList = new ArrayList<>();
                        arrayList = getLatestPostRespose.getVideos();

                        final ArrayList<String> tList = new ArrayList<String>();

                        try {
                            for (int i = 0; i < arrayList.size(); i++) {

                                String title = getLatestPostRespose.getVideos().get(i).getTitle();
                                String image = getLatestPostRespose.getVideos().get(i).getThumbnail_url();
                                String video = getLatestPostRespose.getVideos().get(i).getVideo_url();
                                mList.add(ItemFactory.createItemFromAsset(title, video, image, getActivity(), mVideoPlayerManager));
                                tList.add(title);
                            }
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }

                        LikedVideosAdapter likedVideosAdapter = new LikedVideosAdapter(mVideoPlayerManager, getActivity(), mList, getLatestPostRespose);


                        mRecyclerView.setAdapter(likedVideosAdapter);

                        final StickyRecyclerHeadersDecoration headersDecor = new StickyRecyclerHeadersDecoration(likedVideosAdapter);
                        mRecyclerView.addItemDecoration(headersDecor);

                        StickyRecyclerHeadersTouchListener touchListener =
                                new StickyRecyclerHeadersTouchListener(mRecyclerView, headersDecor);
                        touchListener.setOnHeaderClickListener(
                                new StickyRecyclerHeadersTouchListener.OnHeaderClickListener() {
                                    @Override
                                    public void onHeaderClick(View header, int position, long headerId) {
                                        Toast.makeText(getActivity(), "Header position: " + position + ", id: " + headerId,
                                                Toast.LENGTH_SHORT).show();
                                    }
                                });
                        mRecyclerView.addOnItemTouchListener(touchListener);

                        likedVideosAdapter.registerAdapterDataObserver(new RecyclerView.AdapterDataObserver() {
                            @Override
                            public void onChanged() {
                                headersDecor.invalidateHeaders();
                            }
                        });

                        // Add decoration for dividers between list items
                        mRecyclerView.addItemDecoration(new DividerDecoration(getContext()));
                        mRecyclerView.addOnScrollListener(new RecyclerView.OnScrollListener() {


                            @Override
                            public void onScrollStateChanged(RecyclerView recyclerView, int scrollState) {
                                mScrollState = scrollState;
                                if (scrollState == RecyclerView.SCROLL_STATE_IDLE && !mList.isEmpty()) {

                                    mVideoVisibilityCalculator.onScrollStateIdle(
                                            mItemsPositionGetter,
                                            mLayoutManager.findFirstVisibleItemPosition(),
                                            mLayoutManager.findLastVisibleItemPosition());

                                }
                            }

                            @Override
                            public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
                                try {
                                    if (!mList.isEmpty()) {
                                        mVideoVisibilityCalculator.onScroll(
                                                mItemsPositionGetter,
                                                mLayoutManager.findFirstVisibleItemPosition(),
                                                mLayoutManager.findLastVisibleItemPosition() - mLayoutManager.findFirstVisibleItemPosition() + 1,
                                                mScrollState);
                                    }
                                } catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                            }
                        });
                        mItemsPositionGetter = new RecyclerViewItemPositionGetter(mLayoutManager, mRecyclerView);


                        // SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
                        //SharedPreferences.Editor editor = sharedPref.edit();
                        // editor.putString(CommonConstants.USER_TOKEN, loginResponse.getUser().getUser_token());

                        //editor.commit();


                    }

                }
            }catch (Exception ex){
                ex.printStackTrace();
            }
        }
    }
}
