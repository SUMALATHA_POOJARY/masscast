package com.optime.listviewvideo.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.VideoView;

import com.cloudinary.Cloudinary;
import com.google.gson.Gson;
import com.optime.listviewvideo.R;
import com.optime.listviewvideo.Utility.CommonConstants;
import com.optime.listviewvideo.Utility.Utility;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class UploadActivity extends AppCompatActivity {

    Map config = new HashMap();
    String thumbUrl;
    String videoUrl;
    public String exercise;
    String videoName;
    String videoWeight;
    String videoRep;
    VideoView vvUploadVideo;
    EditText etExercise;
    EditText etLbsLifted;
    EditText etRep;
    Button btnPost;
    String eid;
    public static Cloudinary cloudinary;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_upload);


        vvUploadVideo=(VideoView)findViewById(R.id.vvUploadVideo);
        etExercise=(EditText)findViewById(R.id.etEid);
        etLbsLifted=(EditText)findViewById(R.id.etLbsLifted);
        etRep=(EditText)findViewById(R.id.etRep);
        btnPost=(Button)findViewById(R.id.btnPost);


        config.put("cloud_name", "dsy4wmfx8");     //dsy4wmfx8
        config.put("api_key", "578895876186624");
        config.put("api_secret", "0qKIdTIUx0TYGNg1NgUjuopoagk");

        Utility.cloudinary = new Cloudinary(config);
        Intent intent=getIntent();
        final String imagePath=intent.getStringExtra("imagePath");


        //String videoPath=intent.get

        final File thFile = new File(Utility.THUMBNAIL_DIR + "th_" + System.currentTimeMillis()+"");
        FileOutputStream thOut = null;
        try {
            thOut = new FileOutputStream(thFile);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        //Upload thumbnail
        try {
            Bitmap bitmap = ThumbnailUtils.createVideoThumbnail(imagePath, MediaStore.Images.Thumbnails.MINI_KIND);
            //	Bitmap bitmap = BitmapFactory.decodeResource(getResources(),R.drawable.icon);           ByteArrayOutputStream stream = new ByteArrayOutputStream();
            //  Bitmap bmp = Bitmap.createScaledBitmap(bitmap, 225, 225, true);
            bitmap.compress(Bitmap.CompressFormat.PNG, 85, thOut);
        }catch (Exception ex){
            ex.printStackTrace();
        }

        try {
            thOut.flush();
            thOut.close();
        } catch (IOException e) {
            e.printStackTrace();
        }




        InputStream in = null;String fileName1 = "";


        // if(type == 2) {
        fileName1 = "file_" + System.currentTimeMillis() + "_" + String.valueOf(System
                .currentTimeMillis()) + "";


        try {
            Uri selectedImageUri = Uri.fromFile(new File(thFile.getPath()));
            in = getContentResolver().openInputStream(selectedImageUri);
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }


       // uploadThumbNail(in, fileName1, imagePath, 3);
        final String finalFileName = fileName1;
        btnPost.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                exercise=etExercise.getText().toString();
                videoWeight=etLbsLifted.getText().toString();
                videoRep=etRep.getText().toString();
                vvUploadVideo.setVideoURI(Uri.parse(imagePath));

                if(exercise.equals("Bench")){
                    eid="1";
                }else if(exercise.equals("Squat")){
                    eid="2";
                }else if(exercise.equals("Deadlift")) {
                    eid = "3";
                }else if(exercise.equals("Curls")) {
                    eid = "4";
                }
                else if(exercise.equals("Incline bench")) {
                    eid = "5";
                }

                SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(CommonConstants.VIDEO_EXERCISE_ID, eid);
                editor.putString(CommonConstants.VIDEO_WEIGHT, videoWeight);
                editor.putString(CommonConstants.VIDEO_REP, videoRep);

                editor.commit();


                Utility.upload(UploadActivity.this, finalFileName, imagePath, 2);   //upload video
                Utility.upload(UploadActivity.this, finalFileName, thFile.getPath(), 1);   //upload image
            }
        });


    }

    public void uploadThumbNail(final InputStream inputStream, final String publicId, final String originalPath, final int type) {

        final Map[] uploadResult = new Map[1];
        final String[] thumbnailUrl = new String[1];
        final Map<String, String> options = new HashMap<>();
        //   if(type == 2) {
        options.put("public_id", publicId);
        //}else {

        //  options.put("resource_type", "video");
        //  }

        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    //  if(type == 2) {
                    uploadResult[0] = cloudinary.uploader().upload(inputStream, options);
                    // }else
                    //  {
                    //  uploadResult = cloudinary.uploader().upload(publicId, options);

                    //  }
                    thumbnailUrl[0] = uploadResult[0].get("url").toString();

                    thumbUrl=thumbnailUrl[0];
             //       videoUrl= String.valueOf(uploadResult[0]);
              //      videoName= (String) uploadResult[0].get("url");
               //     videoWeight= (String) uploadResult[0].get("version");


                    // if(type == 2) {
                    // SendImage(uploadUrl, type, originalPath, caption, (int) pid, seqNum);
                    //  }else
                    // {
                    //  SendImage(uploadUrl, type, publicId, (int) pid, seqNum);
                    // }

                    InputStream in = null;
                    String fileName1 = "";


                    // if(type == 2) {
                    fileName1 = "file_" + System.currentTimeMillis() + "_" + String.valueOf(System
                            .currentTimeMillis()) + "";


                    try {
                        Uri selectedImageUri = Uri.fromFile(new File(originalPath));
                        in = getContentResolver().openInputStream(selectedImageUri);
                    } catch (FileNotFoundException e) {
                        e.printStackTrace();
                    }

                   // upload(in, fileName1, thumbnailUrl[0], type);
                    Utility.upload(UploadActivity.this, fileName1, thumbnailUrl[0], type);


                    videoUrl=Utility.VIDEO_URL;




                } catch (IOException e) {
                    //TODO: better error handling when image uploading fails
                    e.printStackTrace();
                }
            }
        };

        new Thread(runnable).start();
    }

    public void upload(final InputStream inputStream, final String publicId, final String thumbnailUrl, int type) {

        final Map[] uploadResult = new Map[1];
        final String[] uploadUrl = new String[1];
        final Map<String, String> options = new HashMap<>();

            options.put("resource_type", "video");



        final int finalType = type;
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                try {
                    //  if(type == 2) {
                    uploadResult[0] = cloudinary.uploader().upload(inputStream, options);
                    // }else
                    //  {
                    //  uploadResult = cloudinary.uploader().upload(publicId, options);

                    //  }
                    uploadUrl[0] = uploadResult[0].get("url").toString();
                    videoUrl=uploadUrl[0];
                    // if(type == 2) {
                    // SendImage(uploadUrl, type, originalPath, caption, (int) pid, seqNum);
                    //  }else
                    // {
                    //  SendImage(uploadUrl, type, publicId, (int) pid, seqNum);
                    // }





                } catch (IOException e) {
                    //TODO: better error handling when image uploading fails
                    e.printStackTrace();
                }catch(OutOfMemoryError e)
                {
                    e.printStackTrace();
                }catch (Exception e)
                {
                    e.printStackTrace();
                }
            }
        };

        new Thread(runnable).start();
    }

    class PostVideo extends AsyncTask<String, Void, String> {

        private Dialog loadingDialog;
        String result = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // loadingDialog = ProgressDialog.show(MainActivity.this, "Please wait", "Loading...");

        }

        @Override
        protected String doInBackground(String... params) {

            try {
                String eid= URLEncoder.encode("1", "UTF-8");
                String title=URLEncoder.encode("video1", "UTF-8");
                String videoUrl1= URLEncoder.encode(videoUrl, "UTF-8");
                String thumb_url1=URLEncoder.encode(thumbUrl, "UTF-8");
                String weight=URLEncoder.encode("10", "UTF-8");
                String rep=URLEncoder.encode("3", "UTF-8");


            InputStream is = null;
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("ent_email", "suma@gmail.com"));
            nameValuePairs.add(new BasicNameValuePair("ent_token", "a0955e1cb6c3599b7db80935c3d718cf"));
            nameValuePairs.add(new BasicNameValuePair("ent_eid", eid));
            nameValuePairs.add(new BasicNameValuePair("ent_title", title));
            nameValuePairs.add(new BasicNameValuePair("ent_url", videoUrl1));
            nameValuePairs.add(new BasicNameValuePair("ent_thumb_url", thumb_url1));
            nameValuePairs.add(new BasicNameValuePair("ent_weight:", weight));
            nameValuePairs.add(new BasicNameValuePair("ent_rep", rep));





                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(
                        "http://optime.in/apps/mass_cast/mass_cast_api.php/insert_video");
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = httpClient.execute(httpPost);

                HttpEntity entity = response.getEntity();

                is = entity.getContent();

                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();

                String line = null;
                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {


                Gson gson = new Gson();
//                UploadVideoResponse uploadVideoResponse = gson.fromJson(result, UploadVideoResponse.class);

 //               if (uploadVideoResponse.getErr().getCode() == 0) {

//                   String videoId=uploadVideoResponse.getVideo_id();



               // }
            }
        }
    }

}
