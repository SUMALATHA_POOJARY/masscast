package com.optime.listviewvideo.Activity;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.TextView;

import com.optime.listviewvideo.Model.Error;
import com.optime.listviewvideo.R;
import com.optime.listviewvideo.Utility.CommonConstants;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.List;

public class SignUpActivity extends AppCompatActivity implements DialogInterface.OnClickListener, View.OnClickListener {


    EditText etUsername;
    EditText etName;
    EditText etEmail;
    EditText etPassword;
    EditText etRePwd;
    Button btnNext;
    TextView tvMale;
    TextView tvFemale;
    TextView tvHeightFeet;
    TextView tvHeightCm;
    TextView tvWeightKg;
    TextView tvWeightLbs;
    EditText etDOB;
    EditText etHeight1;
    EditText etHeight2;
    EditText etHeightCm;
    EditText etWeight;

    String username;
    String name;
    String email;
    String password;
    String rePassword;
    String height1;
    String height2;
    double height;
    double weight;
    String weight1;
    int genderFlag=0;
    static final int DATE_DIALOG_ID = 100;
    private int year;
    private int month;
    private int day;
    int age;
    private Calendar cal;
    int heightFlag=0;
    int weightFlag=0;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);


        etUsername=(EditText)findViewById(R.id.etUser);
        etName=(EditText)findViewById(R.id.etName);
        etEmail=(EditText)findViewById(R.id.etEmail);
        etPassword=(EditText)findViewById(R.id.etPwd);
        etRePwd=(EditText)findViewById(R.id.etRePwd);
        btnNext=(Button)findViewById(R.id.btnNext);
        tvMale=(TextView)findViewById(R.id.tvMale);
        tvFemale=(TextView)findViewById(R.id.tvFemale);
        tvHeightFeet=(TextView)findViewById(R.id.tvHeightFeet);
        tvHeightCm=(TextView)findViewById(R.id.tvHeightCm);
        tvWeightKg=(TextView)findViewById(R.id.tvWeightKg);
        tvWeightLbs=(TextView)findViewById(R.id.tvWeightLbs);
        etDOB=(EditText)findViewById(R.id.etDOB);
        etHeight1=(EditText)findViewById(R.id.etHeight1);
        etHeight2=(EditText)findViewById(R.id.etHeight2);
        etHeightCm=(EditText)findViewById(R.id.etHeightCm);
        etWeight=(EditText)findViewById(R.id.etWeight);


        cal = Calendar.getInstance();
        day = cal.get(Calendar.DAY_OF_MONTH);
        month = cal.get(Calendar.MONTH);
        year = cal.get(Calendar.YEAR);
        etDOB.setOnClickListener(this);
        this.getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_ALWAYS_HIDDEN);
        //etUsername.getNextFocusForwardId()

        etUsername.setOnEditorActionListener(new TextView.OnEditorActionListener() {

            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_SEARCH) {
                    username=etUsername.getText().toString();
                    new CheckAvailability().execute();
                    return true;
                }
                return false;
            }
        });
/*
        etUsername.addTextChangedListener(new TextWatcher() {


            public void afterTextChanged(Editable s) {
                username=etUsername.getText().toString();

                // you can call or do what you want with your EditText here
                new CheckAvailability().execute();


            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

            public void onTextChanged(CharSequence s, int start, int before, int count) {}
        });
*/
        tvFemale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                genderFlag=1;
                tvMale.setBackground(null);
                tvFemale.setBackgroundResource(R.drawable.custombuttonborder);
                SharedPreferences sharedPref = SignUpActivity.this.getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(CommonConstants.USER_SEX, Integer.toString(genderFlag));
            }
        });

        tvMale.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                tvFemale.setBackground(null);
                tvMale.setBackgroundResource(R.drawable.custombuttonborder);
                SharedPreferences sharedPref = SignUpActivity.this.getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
                SharedPreferences.Editor editor = sharedPref.edit();
                editor.putString(CommonConstants.USER_SEX, Integer.toString(genderFlag));
            }
        });
        tvHeightCm.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvHeightFeet.setBackground(null);
                tvHeightCm.setBackgroundResource(R.drawable.custombuttonborder);
                etHeight1.setVisibility(View.GONE);
                etHeight2.setVisibility(View.GONE);
                etHeightCm.setVisibility(View.VISIBLE);
                heightFlag=1;
            }
        });
        tvHeightFeet.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvHeightCm.setBackground(null);
                tvHeightFeet.setBackgroundResource(R.drawable.custombuttonborder);
                etHeight1.setVisibility(View.VISIBLE);
                etHeight2.setVisibility(View.VISIBLE);
                etHeightCm.setVisibility(View.GONE);
                heightFlag=0;
            }
        });
        tvWeightLbs.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvWeightKg.setBackground(null);
                tvWeightLbs.setBackgroundResource(R.drawable.custombuttonborder);
                weightFlag=1;
            }
        });
        tvWeightKg.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                tvWeightLbs.setBackground(null);
                tvWeightKg.setBackgroundResource(R.drawable.custombuttonborder);
                weightFlag=1;
            }
        });



        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                getInputs();

                Intent intent=new Intent(SignUpActivity.this, SignUpProfilePhotoUploadActivity.class);
                startActivity(intent);

            }
        });

    }

    public void getInputs(){


//        new CheckAvailability().execute();

//        etUsername.addTextChangedListener(new MyTextWatcher(etUsername));

/*
        etUsername.setOnEditorActionListener(new TextView.OnEditorActionListener() {


            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                if (actionId == EditorInfo.IME_ACTION_DONE) {

                    new CheckAvailability().execute();

                    return true;
                }
                return false;
            }
        });
*/
        username=etUsername.getText().toString();
        name=etName.getText().toString();
        email=etEmail.getText().toString();
        password=etPassword.getText().toString();
        rePassword=etRePwd.getText().toString();
        height1=etHeight1.getText().toString();
        height2=etHeight2.getText().toString();
        weight1=etWeight.getText().toString();


        try {
            if(heightFlag==0) {
                height = convertFeetandInchesToCentimeter(height1, height2);
            }else{
                  height=Double.parseDouble(height1);
            }

            if(weightFlag==0){
                weight = Double.parseDouble(weight1) * 2.2;
            }
            else{
                weight=Double.parseDouble(weight1);
            }

        } catch (NumberFormatException e) {
          e.printStackTrace(); // your default value
        }

        SharedPreferences sharedPref = SignUpActivity.this.getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(CommonConstants.USER_NAME, name);
        editor.putString(CommonConstants.USER_USER_NAME, username);
        editor.putString(CommonConstants.USER_EMAIL, email);
        editor.putString(CommonConstants.USER_PASSWORD, password);
        editor.putString(CommonConstants.USER_HEIGHT, Double.toString(height));
        editor.putString(CommonConstants.USER_WEIGHT, Double.toString(weight));
        editor.putString(CommonConstants.USER_AGE, Integer.toString(age));


        editor.commit();


    }

    private class MyTextWatcher implements TextWatcher {

        private View view;

        private MyTextWatcher(View view) {
            this.view = view;
        }

        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
        }

        public void afterTextChanged(Editable editable) {
            switch (view.getId()) {
                case R.id.etUserName:
                    new CheckAvailability().execute();
                    break;

            }
        }
    }

    public static double convertFeetandInchesToCentimeter(String feet, String inches) {
        double heightInFeet = 0;
        double heightInInches = 0;
        try {
            if (feet != null && feet.trim().length() != 0) {
                heightInFeet = Double.parseDouble(feet);
            }
            if (inches != null && inches.trim().length() != 0) {
                heightInInches = Double.parseDouble(inches);
            }
        } catch (NumberFormatException nfe) {

        }
        return (heightInFeet * 30.48) + (heightInInches * 2.54);
    }

    public int getAge (int _year, int _month, int _day) {

        GregorianCalendar cal = new GregorianCalendar();
        int y, m, d, a;

        y = cal.get(Calendar.YEAR);
        m = cal.get(Calendar.MONTH);
        d = cal.get(Calendar.DAY_OF_MONTH);
        cal.set(_year, _month, _day);
        a = y - cal.get(Calendar.YEAR);
        if ((m < cal.get(Calendar.MONTH))
                || ((m == cal.get(Calendar.MONTH)) && (d < cal
                .get(Calendar.DAY_OF_MONTH)))) {
            --a;
        }
        if(a < 0)
            throw new IllegalArgumentException("Age < 0");
        return a;
    }

    @Override
    @Deprecated
    protected Dialog onCreateDialog(int id) {
        etDOB.clearFocus();
        return new DatePickerDialog(this, datePickerListener, year, month, day);
    }

    private DatePickerDialog.OnDateSetListener datePickerListener = new DatePickerDialog.OnDateSetListener() {
        public void onDateSet(DatePicker view, int selectedYear,
                              int selectedMonth, int selectedDay) {

            try {
                etDOB.setText(selectedDay + " / " + (selectedMonth + 1) + " / "
                        + selectedYear);
                //etDOB.setText(selectedDay+selectedMonth+selectedYear+"");


                age = getAge(selectedYear, selectedMonth + 1, selectedDay);
            }catch (IllegalArgumentException ex){
                ex.printStackTrace();
            }catch (Exception ex){
                ex.printStackTrace();
            }

            SharedPreferences sharedPref = SignUpActivity.this.getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
            SharedPreferences.Editor editor = sharedPref.edit();
            editor.putString(CommonConstants.USER_AGE, Integer.toString(age));
            editor.commit();


        }
    };

    @Override
    public void onClick(DialogInterface dialog, int which) {

    }

    @Override
    public void onClick(View v) {
        showDialog(0);
    }

    class CheckAvailability extends AsyncTask<String, Void, String> {

        private Dialog loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // loadingDialog = ProgressDialog.show(MainActivity.this, "Please wait", "Loading...");


        }

        @Override
        protected String doInBackground(String... params) {

            InputStream is = null;
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("ent_uname",username));
            String result = null;

            try{
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(
                        "http://optime.in/apps/mass_cast/mass_cast_api.php/check_availability");
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = httpClient.execute(httpPost);

                HttpEntity entity = response.getEntity();

                is = entity.getContent();

                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();

                String line = null;
                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {


                com.optime.listviewvideo.Model.Error er=new Error();
                if(er.getCode()!=0){

                    etUsername.setBackgroundResource(R.color.appRed);

                }
                else{

                    /*
                    AlertDialog.Builder dlgAlert  = new AlertDialog.Builder(SignUpActivity.this);

                    dlgAlert.setMessage("wrong password or username");
                    dlgAlert.setTitle("Error Message...");
                    dlgAlert.setPositiveButton("OK", null);
                    dlgAlert.setCancelable(true);
                    dlgAlert.create().show();

                    dlgAlert.setPositiveButton("Ok",
                            new DialogInterface.OnClickListener() {
                                public void onClick(DialogInterface dialog, int which) {

                                }
                            });*/
                }

            }
        }
    }


}
