package com.optime.listviewvideo.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.MediaStore;
import android.provider.Settings;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.cloudinary.Cloudinary;
import com.google.gson.Gson;
import com.optime.listviewvideo.Model.SignupJson;
import com.optime.listviewvideo.R;
import com.optime.listviewvideo.Utility.CommonConstants;
import com.optime.listviewvideo.Utility.Utility;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class SignUpProfilePhotoUploadActivity extends AppCompatActivity {

    Button btnSignUp;
    String android_id;
    String username;
    String name;
    String email;
    String password;
    String gender;
    String deviceToken="aaa";
    Dialog dialog;
    Map config = new HashMap();
    ImageView ivProfilePhoto;
    String imageUrl;
    String height;
    String weight;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up_profile_photo_upload);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        btnSignUp=(Button)findViewById(R.id.btnSignUp);
        ivProfilePhoto=(ImageView)findViewById(R.id.ivProfilePhoto);

        config.put("cloud_name", "dsy4wmfx8");     //dsy4wmfx8
        config.put("api_key", "578895876186624");
        config.put("api_secret", "0qKIdTIUx0TYGNg1NgUjuopoagk");

        Utility.cloudinary = new Cloudinary(config);


        android_id = Settings.Secure.getString(SignUpProfilePhotoUploadActivity.this.getContentResolver(),
                Settings.Secure.ANDROID_ID);


        SharedPreferences prefs = SignUpProfilePhotoUploadActivity.this.getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
        email= prefs.getString(CommonConstants.USER_EMAIL, "");
        username= prefs.getString(CommonConstants.USER_USER_NAME, "");
        password= prefs.getString(CommonConstants.USER_PASSWORD, "");
        name= prefs.getString(CommonConstants.USER_NAME, "");
        gender= prefs.getString(CommonConstants.USER_SEX,"");
        imageUrl= prefs.getString(CommonConstants.PROFILE_PHOTO_URL,"");
        height= prefs.getString(CommonConstants.USER_HEIGHT,"");
        weight= prefs.getString(CommonConstants.USER_WEIGHT,"");



        ivProfilePhoto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                uploadProfilePhoto();
            }
        });





        btnSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                new SignUpAsync().execute();

            }
        });


    }

    class SignUpAsync extends AsyncTask<String, Void, String> {

        private Dialog loadingDialog;
        String result = null;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // loadingDialog = ProgressDialog.show(MainActivity.this, "Please wait", "Loading...");
        }

        @Override
        protected String doInBackground(String... params) {

            InputStream is = null;
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("ent_email", email));
            nameValuePairs.add(new BasicNameValuePair("ent_password", password));
            nameValuePairs.add(new BasicNameValuePair("ent_name", name));
            nameValuePairs.add(new BasicNameValuePair("ent_age", "50"));
            nameValuePairs.add(new BasicNameValuePair("ent_sex", gender));
            nameValuePairs.add(new BasicNameValuePair("ent_device_id", android_id + ""));
            nameValuePairs.add(new BasicNameValuePair("ent_device_type", "1"));
            nameValuePairs.add(new BasicNameValuePair("ent_device_token", deviceToken));
            nameValuePairs.add(new BasicNameValuePair("ent_image", imageUrl));
            nameValuePairs.add(new BasicNameValuePair("ent_height", height));
            nameValuePairs.add(new BasicNameValuePair("ent_weight", weight));
            nameValuePairs.add(new BasicNameValuePair(" ent_uname", username));


            try {
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(
                        "http://optime.in/apps/mass_cast/mass_cast_api.php/sign_up");
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = httpClient.execute(httpPost);

                HttpEntity entity = response.getEntity();

                is = entity.getContent();

                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();

                String line = null;
                while ((line = reader.readLine()) != null) {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {

                Gson gson = new Gson();
                SignupJson signupJson = gson.fromJson(result, SignupJson.class);


                if(signupJson.getErr().getCode() == 0) {

                    SharedPreferences sharedPref = SignUpProfilePhotoUploadActivity.this.getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(CommonConstants.USER_SESSION_TOKEN, signupJson.getToken());
                    editor.putString(CommonConstants.USER_DEVICE_TOKEN, deviceToken);
                    editor.commit();

                    Intent intent=new Intent(SignUpProfilePhotoUploadActivity.this, HomePageActivity.class);
                    startActivity(intent);



                }

            }
        }
    }

    public void uploadProfilePhoto(){
        dialog = new Dialog(SignUpProfilePhotoUploadActivity.this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //tell the Dialog to use the dialog.xml as it's layout description

        dialog.setContentView(R.layout.gallery_camera_popup);
        dialog.setCancelable(true);

        RelativeLayout galleryLayout = (RelativeLayout) dialog.findViewById(R.id.gallery_layout);
        RelativeLayout cameraLayout = (RelativeLayout) dialog.findViewById(R.id.camera_layout);
        Button btnCancel=(Button) dialog.findViewById(R.id.btnCancel);

        galleryLayout.setOnClickListener(new RelativeLayout.OnClickListener() {
                                             @Override
                                             public void onClick(View v) {



                                                 if (ContextCompat.checkSelfPermission(SignUpProfilePhotoUploadActivity.this,
                                                         Manifest.permission.READ_EXTERNAL_STORAGE)
                                                         != PackageManager.PERMISSION_GRANTED) {

                                                     // Should we show an explanation?
                                                     if (ActivityCompat.shouldShowRequestPermissionRationale(SignUpProfilePhotoUploadActivity.this,
                                                             Manifest.permission.READ_EXTERNAL_STORAGE)) {

                                                         // Show an expanation to the user *asynchronously* -- don't block
                                                         // this thread waiting for the user's response! After the user
                                                         // sees the explanation, try again to request the permission.

                                                     } else {

                                                         // No explanation needed, we can request the permission.

                                                         ActivityCompat.requestPermissions(SignUpProfilePhotoUploadActivity.this,
                                                                 new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                                                 1);


                                                         // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                                                         // app-defined int constant. The callback method gets the
                                                         // result of the request.
                                                     }
                                                 } else {


                                                     Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT,
                                                             android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                                     galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                                                     // Start the Intent

                                                     //Intent i = new Intent(Action.ACTION_MULTIPLE_PICK);
                                                     startActivityForResult(galleryIntent, 1);
                                                     dialog.dismiss();

                                                 }
                                             }



                                         }

        );

        cameraLayout.setOnClickListener(new RelativeLayout.OnClickListener()

                                        {
                                            @Override
                                            public void onClick (View v){
                                                // dialog.dismiss();





                            /*if (ContextCompat.checkSelfPermission(getActivity(),
                                    Manifest.permission.CAMERA)
                                    != PackageManager.PERMISSION_GRANTED) {

                                // Should we show an explanation?
                                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                                        Manifest.permission.CAMERA)) {

                                    // Show an expanation to the user *asynchronously* -- don't block
                                    // this thread waiting for the user's response! After the user
                                    // sees the explanation, try again to request the permission.

                                } else {

                                    // No explanation needed, we can request the permission.

                                    ActivityCompat.requestPermissions(getActivity(),
                                            new String[]{Manifest.permission.CAMERA},
                                            2);

                                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                                    // app-defined int constant. The callback method gets the
                                    // result of the request.
                                }
                            } */

                                                Intent intent1 = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
                                                File f = new File(android.os.Environment
                                                        .getExternalStorageDirectory(), "temp.png");
                                                startActivityForResult(intent1, 1);

                                            }

                                        }

        );

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == 1 && resultCode == Activity.RESULT_OK
                    && null != data) {


                String[] filePathColumn = { MediaStore.Images.Media.DATA };






                if(data.getData()!=null){

                    Uri mImageUri=data.getData();

                    // Get the cursor

                    String selectedImage = getPath(mImageUri);
                    Cursor cursor = SignUpProfilePhotoUploadActivity.this.getContentResolver().query(mImageUri,
                            filePathColumn, null, null, null);
                    // Move to first row
                    cursor.moveToFirst();

                    ivProfilePhoto.setImageURI(Uri.parse(selectedImage));
                     String fileName = "file_" + System.currentTimeMillis() + "_" + String.valueOf(System
                            .currentTimeMillis()) + "";

                    Utility.uploadProfilePhoto(SignUpProfilePhotoUploadActivity.this, fileName, selectedImage, 1);


                }


            }else if (requestCode == 2) {


                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");

                Uri tempUri = getImageUri(SignUpProfilePhotoUploadActivity.this.getApplicationContext(), imageBitmap);

                // CALL THIS METHOD TO GET THE ACTUAL PATH
                File finalFile = new File(getRealPathFromURI(tempUri));
                File f = finalFile;






            }
            else {
                Toast.makeText(SignUpProfilePhotoUploadActivity.this, "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(SignUpProfilePhotoUploadActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = SignUpProfilePhotoUploadActivity.this.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public String getPath(Uri uri)
    {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = SignUpProfilePhotoUploadActivity.this.managedQuery(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        return s;
    }
}
