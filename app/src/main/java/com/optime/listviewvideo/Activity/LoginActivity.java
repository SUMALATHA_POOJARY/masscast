package com.optime.listviewvideo.Activity;

import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.google.gson.Gson;
import com.optime.listviewvideo.Model.LoginResponse;
import com.optime.listviewvideo.R;
import com.optime.listviewvideo.Utility.CommonConstants;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.entity.UrlEncodedFormEntity;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicNameValuePair;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.List;

public class LoginActivity extends AppCompatActivity {

    String android_id;
    String token;
    TextView tvSignUp;
    EditText etUserName;
    EditText etPassword;
    Button btnSignin;
    String username;
    String password;
    String email;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        tvSignUp=(TextView)findViewById(R.id.tvLoginSignup);
        etUserName=(EditText)findViewById(R.id.etUserName);
        etPassword=(EditText)findViewById(R.id.etPassword);
        btnSignin=(Button)findViewById(R.id.btnSignIn);


        btnSignin.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // username=etUserName.getText().toString();
               // password=etPassword.getText().toString();

                SharedPreferences prefs = LoginActivity.this.getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
                token= prefs.getString(CommonConstants.USER_DEVICE_TOKEN, "");
                password= prefs.getString(CommonConstants.USER_PASSWORD, "");
                email= prefs.getString(CommonConstants.USER_EMAIL, "");
                username= prefs.getString(CommonConstants.USER_USER_NAME,"");

                new LoginAsync().execute();
            }
        });


        tvSignUp.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignUpActivity.class);
                startActivity(intent);
            }
        });




    }

    class LoginAsync extends AsyncTask<String, Void, String> {

        private Dialog loadingDialog;

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            // loadingDialog = ProgressDialog.show(MainActivity.this, "Please wait", "Loading...");

            android_id = Settings.Secure.getString(LoginActivity.this.getContentResolver(),
                    Settings.Secure.ANDROID_ID);
        }

        @Override
        protected String doInBackground(String... params) {

            InputStream is = null;
            List<NameValuePair> nameValuePairs = new ArrayList<NameValuePair>();
            nameValuePairs.add(new BasicNameValuePair("ent_uname",username));
            nameValuePairs.add(new BasicNameValuePair("ent_password", password));
            nameValuePairs.add(new BasicNameValuePair("ent_device_id", android_id));
            nameValuePairs.add(new BasicNameValuePair("ent_device_type", "1"));
            nameValuePairs.add(new BasicNameValuePair("ent_device_token", token));
            String result = null;

            try{
                HttpClient httpClient = new DefaultHttpClient();
                HttpPost httpPost = new HttpPost(
                        "http://optime.in/apps/mass_cast/mass_cast_api.php/login");
                httpPost.setEntity(new UrlEncodedFormEntity(nameValuePairs));

                HttpResponse response = httpClient.execute(httpPost);

                HttpEntity entity = response.getEntity();

                is = entity.getContent();

                BufferedReader reader = new BufferedReader(new InputStreamReader(is, "UTF-8"), 8);
                StringBuilder sb = new StringBuilder();

                String line = null;
                while ((line = reader.readLine()) != null)
                {
                    sb.append(line + "\n");
                }
                result = sb.toString();
            } catch (ClientProtocolException e) {
                e.printStackTrace();
            } catch (UnsupportedEncodingException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return result;
        }

        @Override
        protected void onPostExecute(String result) {

            if (result != null) {


                Gson gson = new Gson();
                LoginResponse loginResponse = gson.fromJson(result, LoginResponse.class);

                if (loginResponse.getErr().getCode() == 0) {


                    SharedPreferences sharedPref = getApplicationContext().getSharedPreferences(CommonConstants.USER_SETTINGS_PREFERENCE, Context.MODE_PRIVATE);
                    SharedPreferences.Editor editor = sharedPref.edit();
                    editor.putString(CommonConstants.USER_SESSION_TOKEN, loginResponse.getToken());

                    editor.commit();


                    Intent intent=new Intent(LoginActivity.this, HomePageActivity.class);
                    startActivity(intent);


                }
            }
        }
    }

}
