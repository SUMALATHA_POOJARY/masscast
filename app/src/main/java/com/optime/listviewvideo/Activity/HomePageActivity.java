package com.optime.listviewvideo.Activity;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.design.widget.TabLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.RelativeLayout;
import android.widget.Toast;

import com.optime.listviewvideo.Fragment.GetLikedVideosFragment;
import com.optime.listviewvideo.Fragment.MyProfileFragment;
import com.optime.listviewvideo.Fragment.RankFragment;
import com.optime.listviewvideo.Fragment.UploadVideoFragment;
import com.optime.listviewvideo.Fragment.VideoRecyclerViewFragment;
import com.optime.listviewvideo.R;
import com.optime.listviewvideo.Utility.Utility;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class HomePageActivity extends AppCompatActivity {

    TabLayout tabLayout;
    Dialog dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.content_home_page);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        final ViewPager viewPager = (ViewPager) findViewById(R.id.viewPager);
        if (viewPager != null) {
            setupViewPager(viewPager);
        }
        tabLayout = (TabLayout) findViewById(R.id.tabs);
        tabLayout.setupWithViewPager(viewPager);
        tabLayout.setTabGravity(TabLayout.GRAVITY_CENTER);

        final int[] ICONS = new int[]{
                R.drawable.home,
                R.drawable.rank,
                R.drawable.add,
                R.drawable.like,
                R.drawable.setting};



        tabLayout.getTabAt(0).setIcon(ICONS[0]);
        tabLayout.getTabAt(1).setIcon(ICONS[1]);
        tabLayout.getTabAt(2).setIcon(ICONS[2]);
        tabLayout.getTabAt(3).setIcon(ICONS[3]);
        tabLayout.getTabAt(4).setIcon(ICONS[4]);


        tabLayout.setOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {

                if(tab.getPosition()==2){
                    uploadVideo();
                }

                viewPager.setCurrentItem(tab.getPosition());
                //  setTopData(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {

            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {

            }
        });
/*
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, new VideoRecyclerViewFragment())
                    .commit();
        }
*/

        File dir = new File(Utility.CM_DIR);
        try {
            if (dir.mkdir()) {
                File image = new File(Utility.IMAGE_DIR);
                image.mkdir();
                File video = new File(Utility.VIDEO_DIR);
                video.mkdir();
                File thumbNail = new File(Utility.THUMBNAIL_DIR);
                thumbNail.mkdir();

                System.out.println("Directory created");
            } else {
                System.out.println("Directory is not created");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }

    private void setupViewPager(ViewPager viewPager) {
        Adapter adapter = new Adapter(getSupportFragmentManager());
        adapter.addFragment(new VideoRecyclerViewFragment(), "");
        adapter.addFragment(new RankFragment(), "");
        adapter.addFragment(new UploadVideoFragment(), "");
        adapter.addFragment(new GetLikedVideosFragment(), "");
        adapter.addFragment(new MyProfileFragment(), "");

        //adapter.addFragment(new NationalNewsFragment(), "  NATIONAL NEWS  ");
        //adapter.addFragment(new JobFragment(), "  JOBS  ");
        //adapter.addFragment(new MoneyFragment(), "  MONEY  ");
        //adapter.addFragment(new MediaFragment(), "  REAL ESTATE  ");
        //adapter.addFragment(new CorporateNewsFragment(), "  CORPORATE NEWS  ");
        viewPager.setAdapter(adapter);
    }


    static class Adapter extends FragmentPagerAdapter {
        private final List<Fragment> mFragments = new ArrayList<>();
        private final List<String> mFragmentTitles = new ArrayList<>();


        public Adapter(FragmentManager fm) {
            super(fm);
        }

        public void addFragment(Fragment fragment, String title) {
            mFragments.add(fragment);
            mFragmentTitles.add(title);

        }

        @Override
        public Fragment getItem(int position) {
            return mFragments.get(position);
        }

        @Override
        public int getCount() {
            return mFragments.size();
        }

        @Override
        public CharSequence getPageTitle(int position) {
            return mFragmentTitles.get(position);
        }
    }

    public void uploadVideo(){
        dialog = new Dialog(HomePageActivity.this);

        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

        //tell the Dialog to use the dialog.xml as it's layout description

        dialog.setContentView(R.layout.gallery_camera_popup);
        dialog.setCancelable(true);

        RelativeLayout galleryLayout = (RelativeLayout) dialog.findViewById(R.id.gallery_layout);
        RelativeLayout cameraLayout = (RelativeLayout) dialog.findViewById(R.id.camera_layout);
        Button btnCancel=(Button) dialog.findViewById(R.id.btnCancel);

        galleryLayout.setOnClickListener(new RelativeLayout.OnClickListener() {
                                             @Override
                                             public void onClick(View v) {



                                                 if (ContextCompat.checkSelfPermission(HomePageActivity.this,
                                                         Manifest.permission.READ_EXTERNAL_STORAGE)
                                                         != PackageManager.PERMISSION_GRANTED) {

                                                     // Should we show an explanation?
                                                     if (ActivityCompat.shouldShowRequestPermissionRationale(HomePageActivity.this,
                                                             Manifest.permission.READ_EXTERNAL_STORAGE)) {

                                                         // Show an expanation to the user *asynchronously* -- don't block
                                                         // this thread waiting for the user's response! After the user
                                                         // sees the explanation, try again to request the permission.

                                                     } else {

                                                         // No explanation needed, we can request the permission.

                                                         ActivityCompat.requestPermissions(HomePageActivity.this,
                                                                 new String[]{Manifest.permission.READ_EXTERNAL_STORAGE},
                                                                 1);


                                                         // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                                                         // app-defined int constant. The callback method gets the
                                                         // result of the request.
                                                     }
                                                 } else {


                                                     Intent galleryIntent = new Intent(Intent.ACTION_GET_CONTENT,
                                                             android.provider.MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                                                     galleryIntent.putExtra(Intent.EXTRA_ALLOW_MULTIPLE, true);
                                                     // Start the Intent

                                                     //Intent i = new Intent(Action.ACTION_MULTIPLE_PICK);
                                                     startActivityForResult(galleryIntent, 1);
                                                     dialog.dismiss();

                                                 }
                                             }



                                         }

        );

        cameraLayout.setOnClickListener(new RelativeLayout.OnClickListener()

                                        {
                                            @Override
                                            public void onClick (View v){
                                                // dialog.dismiss();





                            /*if (ContextCompat.checkSelfPermission(getActivity(),
                                    Manifest.permission.CAMERA)
                                    != PackageManager.PERMISSION_GRANTED) {

                                // Should we show an explanation?
                                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(),
                                        Manifest.permission.CAMERA)) {

                                    // Show an expanation to the user *asynchronously* -- don't block
                                    // this thread waiting for the user's response! After the user
                                    // sees the explanation, try again to request the permission.

                                } else {

                                    // No explanation needed, we can request the permission.

                                    ActivityCompat.requestPermissions(getActivity(),
                                            new String[]{Manifest.permission.CAMERA},
                                            2);

                                    // MY_PERMISSIONS_REQUEST_READ_CONTACTS is an
                                    // app-defined int constant. The callback method gets the
                                    // result of the request.
                                }
                            } */

                                                Intent intent1 = new Intent(MediaStore.ACTION_VIDEO_CAPTURE);
                                                File f = new File(android.os.Environment
                                                        .getExternalStorageDirectory(), "temp.mp4");
                                                startActivityForResult(intent1, 1);

                                            }

                                        }

        );

        btnCancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        dialog.show();
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        try {
            // When an Image is picked
            if (requestCode == 1 && resultCode == Activity.RESULT_OK
                    && null != data) {


                String[] filePathColumn = { MediaStore.Images.Media.DATA };






                if(data.getData()!=null){

                    Uri mImageUri=data.getData();

                    // Get the cursor

                    String selectedImage = getPath(mImageUri);
                    Cursor cursor = HomePageActivity.this.getContentResolver().query(mImageUri,
                            filePathColumn, null, null, null);

                    Intent intent=new Intent(HomePageActivity.this, UploadActivity.class);
                    intent.putExtra("imagePath", selectedImage);
                    startActivity(intent);
                    // Move to first row
                    cursor.moveToFirst();



                }


            }else if (requestCode == 2) {


                Bundle extras = data.getExtras();
                Bitmap imageBitmap = (Bitmap) extras.get("data");

                Uri tempUri = getImageUri(HomePageActivity.this.getApplicationContext(), imageBitmap);

                // CALL THIS METHOD TO GET THE ACTUAL PATH
                File finalFile = new File(getRealPathFromURI(tempUri));
                File f = finalFile;






            }
            else {
                Toast.makeText(HomePageActivity.this, "You haven't picked Image", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(HomePageActivity.this, "Something went wrong", Toast.LENGTH_LONG).show();
            e.printStackTrace();
        }

    }

    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }

    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = HomePageActivity.this.getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }

    public String getPath(Uri uri)
    {
        String[] projection = { MediaStore.Images.Media.DATA };
        Cursor cursor = HomePageActivity.this.managedQuery(uri, projection, null, null, null);
        if (cursor == null) return null;
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();
        String s=cursor.getString(column_index);
        return s;
    }

}
