package com.optime.listviewvideo.VideoPlayerModel;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.optime.listviewvideo.R;
import com.volokh.danylo.video_player_manager.ui.VideoPlayerView;


public class VideoViewHolder extends RecyclerView.ViewHolder{

    public final VideoPlayerView mPlayer;
   // public final TextView mTitle;
    public final ImageView mCover;
    //public final TextView mVisibilityPercents;
    //public final TextView titlevideo1;

    //TextView textView;

    TextView tvName;
    TextView tvWeight;
    TextView tvLbsLifted;
    TextView tvRep;
    ImageView ivProfilePhoto;
    public ImageView ivLike;
    public ImageView ivComment;
    public TextView tvLikeCount;
    public TextView tvCommentCount;


    public VideoViewHolder(View view) {
        super(view);
        mPlayer = (VideoPlayerView) view.findViewById(R.id.player);
       // mTitle = (TextView) view.findViewById(R.id.title);
        mCover = (ImageView) view.findViewById(R.id.cover);
        //mVisibilityPercents = (TextView) view.findViewById(R.id.visibility_percents);

       // titlevideo1 = (TextView) view.findViewById(R.id.titleVideo1);

        //TextView textView = (TextView) view.findViewById(R.id.view_header_title);

        tvName=(TextView)view.findViewById(R.id.view_header_title);
        tvWeight=(TextView)view.findViewById(R.id.tvWeight);
        tvLbsLifted=(TextView)view.findViewById(R.id.tvLbsLifted);
        tvRep=(TextView)view.findViewById(R.id.tvReps);
        ivProfilePhoto=(ImageView)view.findViewById(R.id.ivProfilePhoto);

        tvLikeCount=(TextView)view.findViewById(R.id.tvLikeCount);
        tvCommentCount=(TextView)view.findViewById(R.id.tvCommentCount);
        ivLike=(ImageView)view.findViewById(R.id.ivLike);
        ivComment=(ImageView)view.findViewById(R.id.ivComment);

    }

}
