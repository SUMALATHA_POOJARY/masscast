package com.optime.listviewvideo.VideoPlayerModel;

import android.os.Bundle;
import android.support.v7.app.ActionBarActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;

import com.optime.listviewvideo.Fragment.VideoRecyclerViewFragment;
import com.optime.listviewvideo.R;
import com.volokh.danylo.visibility_utils.utils.Config;

public class VideoList extends ActionBarActivity implements VisibilityUtilsFragment.VisibilityUtilsCallback {

    private static final boolean SHOW_LOGS = Config.SHOW_LOGS;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_video_list);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        //final ArrayList<BaseVideoItem> mList = new ArrayList<>();


        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.fragment_container, new VideoRecyclerViewFragment())
                    .commit();
        }


    }

    @Override
    public void setTitle(String title) {

    }





    private void addRecyclerView() {
        getSupportFragmentManager().beginTransaction()
                .replace(R.id.fragment_container, new VideoRecyclerViewFragment())
                .commit();
        //mToolbar.setTitle("Recycler View");
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.enable_list_view:
                if(!item.isChecked()){
                    //addListView();
                }
                break;
            case R.id.enable_recycler_view:
                if(!item.isChecked()){
                    addRecyclerView();
                }
                break;
            case R.id.enable_visibility_utils_demo:
                if(!item.isChecked()){
                    //addVisibilityUtilsFragment();
                }
                break;
            case R.id.enable_video_player_manager_demo:
                if(!item.isChecked()){
                    //addVideoPlayerManagerFragment();
                }
                break;
        }
        item.setChecked(!item.isChecked());

        return true;
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_video_list, menu);
        return true;
    }
}
