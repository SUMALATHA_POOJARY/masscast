package com.optime.listviewvideo.VideoPlayerModel;

import android.view.View;

import com.squareup.picasso.Picasso;
import com.volokh.danylo.video_player_manager.Config;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;
import com.volokh.danylo.video_player_manager.meta.MetaData;
import com.volokh.danylo.video_player_manager.ui.VideoPlayerView;
import com.volokh.danylo.video_player_manager.utils.Logger;

public class AssetVideoItem extends BaseVideoItem {

    private static final String TAG = AssetVideoItem.class.getSimpleName();
    private static final boolean SHOW_LOGS = Config.SHOW_LOGS;

    private final String mAssetFileDescriptor;
    //private final String mTitle;
    private final String titleVideo;

    private final Picasso mImageLoader;
    private final String mImageResource;

    public AssetVideoItem(String title, String assetFileDescriptor, VideoPlayerManager<MetaData> videoPlayerManager, Picasso imageLoader, String imageResource) {
        super(videoPlayerManager);
        //mTitle = title;
        mAssetFileDescriptor = assetFileDescriptor;
        mImageLoader = imageLoader;
        titleVideo = title;
        mImageResource = imageResource;
    }

    @Override
    public void update(int position, final VideoViewHolder viewHolder, VideoPlayerManager videoPlayerManager) {
        if(SHOW_LOGS) Logger.v(TAG, "update, position " + position);

      //  viewHolder.mTitle.setText(titleVideo);
        viewHolder.mCover.setVisibility(View.VISIBLE);
        mImageLoader.load(mImageResource).fit().into(viewHolder.mCover);
        //viewHolder.titlevideo1.setText(titleVideo);

    }


    @Override
    public void playNewVideo(MetaData currentItemMetaData, VideoPlayerView player, VideoPlayerManager<MetaData> videoPlayerManager) {
        videoPlayerManager.playNewVideo(currentItemMetaData, player, mAssetFileDescriptor);
    }

    @Override
    public void stopPlayback(VideoPlayerManager videoPlayerManager) {
        videoPlayerManager.stopAnyPlayback();
    }

    @Override
    public String toString() {
        return getClass() + ", mTitle[" + titleVideo + "]";
    }
}
