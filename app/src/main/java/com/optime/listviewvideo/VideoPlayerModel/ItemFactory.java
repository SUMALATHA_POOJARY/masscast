package com.optime.listviewvideo.VideoPlayerModel;

import android.app.Activity;

import com.squareup.picasso.Picasso;
import com.volokh.danylo.video_player_manager.manager.VideoPlayerManager;
import com.volokh.danylo.video_player_manager.meta.MetaData;

import java.io.IOException;

public class ItemFactory {

    public static BaseVideoItem createItemFromAsset(String assetName, String video, String imageResource, Activity activity, VideoPlayerManager<MetaData> videoPlayerManager) throws IOException {
        //return new AssetVideoItem(assetName, activity.getAssets().openFd(assetName), videoPlayerManager, Picasso.with(activity), imageResource);
        return new AssetVideoItem(assetName, video, videoPlayerManager, Picasso.with(activity), imageResource);
    }
}
